import siteMetadata from '@data/siteMetadata';
import SocialIcon from '@components/social-icons';
import Link from './Link.js';
import { useTranslation } from 'next-i18next';

export default function Footer() {
  const { t } = useTranslation('footer');

  return (
    <footer>
      <div className="flex flex-col items-center mt-16">
        <div className="flex mb-3 space-x-4">
          <SocialIcon
            kind="mail"
            href={`mailto:${siteMetadata.email}`}
            size="6"
          />
          <SocialIcon kind="gitlab" href={siteMetadata.gitlab} size="6" />
          <SocialIcon kind="linkedin" href={siteMetadata.linkedin} size="6" />
          <SocialIcon kind="twitter" href={siteMetadata.twitter} size="6" />
        </div>
        <div className="flex mb-2 text-sm text-gray-500 space-x-2 dark:text-gray-400">
          <Link href="/">
            {siteMetadata.title} {'collective'}
          </Link>
        </div>
        <div className="mb-2 text-sm text-gray-500 dark:text-gray-400">
          <div>
            {t('footer')}{' '}
            <Link href="https://tailwindcss.com/">TailwindCSS,</Link>
            <Link href="https://nextjs.org/"> NextJS</Link> &{' '}
            <Link href="https://www.crypteriat.org">{siteMetadata.author}</Link>
            {`© ${new Date().getFullYear()}`}
          </div>
        </div>
      </div>
    </footer>
  );
}
