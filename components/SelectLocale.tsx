import { useState } from 'react';
import Link from 'next/link';
import siteMetadata from '../data/siteMetadata';
import useRouterContext from '../context/RouterContext';

type Locale = typeof siteMetadata.locales[0];

function SelectLocale() {
  const [show, showItems] = useState(false);

  const router = useRouterContext();
  const locale = router.locale;

  return (
    <div role="menu">
      {show ? null : (
        <button
          className="p-1 text-sm text-blue-400 bg-gray-100 border-2 border-gray-300 dark:border-gray-200 hover:text-black dark:hover:text-white dark:text-blue-700 dark:bg-gray-400 rounded-xl"
          onClick={() => showItems(true)}
        >
          {
            Object.values(
              siteMetadata.locales.find((i) => Object.keys(i)[0] === locale)
            )[0]
          }
        </button>
      )}
      {show ? (
        <div role="none" className="origin-top-3">
          {siteMetadata.locales.map((entry, i) => (
            <Link
              key={i}
              href={router.pathname}
              passHref //required?
              locale={Object.keys(entry as Locale)[0]}
            >
              <button
                className="block w-24 px-4 py-2 text-center text-gray-800 border-2 dark:hover:bg-gray-600 hover:bg-gray-200 dark:text-blue-200 text-md"
                role="menuitem"
                tabIndex={-1}
                key={entry[0]}
                onClick={() => {
                  showItems(false);
                }}
              >
                {Object.values(entry as Locale)[0]}
              </button>
            </Link>
          ))}
        </div>
      ) : null}
    </div>
  );
}

export default SelectLocale;
