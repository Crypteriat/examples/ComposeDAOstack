import React, { useCallback, useEffect, useReducer, useState } from 'react';
import Image from 'next/image';
import providerContext from 'context/ProviderContext';
import { useTranslation } from 'react-i18next';
// import { getNftCollection } from '../nft/NftCollection';

type StateType = {
  provider?: any;
  web3Provider?: any;
  address?: string;
  chainId?: number;
};

type ActionType =
  | {
      type: 'SET_WEB3_PROVIDER';
      provider?: StateType['provider'];
      web3Provider?: StateType['web3Provider'];
      address?: StateType['address'];
      chainId?: StateType['chainId'];
    }
  | {
      type: 'SET_ADDRESS';
      address?: StateType['address'];
    }
  | {
      type: 'SET_CHAIN_ID';
      chainId?: StateType['chainId'];
    }
  | {
      type: 'RESET_WEB3_PROVIDER';
    };

const initialState: StateType = {
  provider: null,
  web3Provider: null,
  address: null,
  chainId: null,
};

function reducer(state: StateType, action: ActionType): StateType {
  switch (action.type) {
    case 'SET_WEB3_PROVIDER':
      return {
        ...state,
        provider: action.provider,
        web3Provider: action.web3Provider,
        address: action.address,
        chainId: action.chainId,
      };
    case 'SET_ADDRESS':
      return {
        ...state,
        address: action.address,
      };
    case 'SET_CHAIN_ID':
      return {
        ...state,
        chainId: action.chainId,
      };
    case 'RESET_WEB3_PROVIDER':
      return initialState;
    default:
      throw new Error();
  }
}

export const Web3ModalComponent = (): JSX.Element => {
  // const [nftCollection, setNftCollection] = useState([{}]);
  const [state, dispatch] = useReducer(reducer, initialState);
  const { provider, web3Provider, address, chainId } = state;
  const { t } = useTranslation('web3modal');
  const web3Modal = providerContext.getModal();

  const connect = useCallback(async function () {
    const provider = await providerContext.getProvider();
    const web3Provider = await providerContext.getWeb3Provider();

    // const signer = providerContext.getSigner();
    const address = await providerContext.getAddress();
    const network = await providerContext.getNetwork();

    dispatch({
      type: 'SET_WEB3_PROVIDER',
      provider,
      web3Provider,
      address,
      chainId: network.chainId,
    });
  }, []);

  const disconnect = useCallback(
    async function () {
      web3Modal.clearCachedProvider();
      if (provider?.disconnect && typeof provider.disconnect === 'function') {
        await provider.disconnect();
      }
      dispatch({
        type: 'RESET_WEB3_PROVIDER',
      });
    },
    [provider]
  );

  // Auto connect to the cached provider
  useEffect(() => {
    if (web3Modal.cachedProvider) {
      connect();
    }
  }, [connect]);

  // A `provider` should come with EIP-1193 events. We'll listen for those events
  // here so that when a user switches accounts or networks, we can update the
  // local React state with that new information.
  useEffect(() => {
    if (provider?.on) {
      const handleAccountsChanged = (accounts: string[]) => {
        // eslint-disable-next-line no-console
        console.log('accountsChanged', accounts);
        dispatch({
          type: 'SET_ADDRESS',
          address: accounts[0],
        });
      };

      const handleChainChanged = (accounts: string[]) => {
        // eslint-disable-next-line no-console
        console.log('accountsChanged', accounts);
        dispatch({
          type: 'SET_ADDRESS',
          address: accounts[0],
        });
      };

      const handleDisconnect = (error: { code: number; message: string }) => {
        // eslint-disable-next-line no-console
        console.log('disconnect', error);
        disconnect();
      };

      provider.on('accountsChanged', handleAccountsChanged);
      provider.on('chainChanged', handleChainChanged);
      provider.on('disconnect', handleDisconnect);

      // const NFTMetadata = async () => {
      //   const collection = await getNftCollection(address, chainId);
      //   setNftCollection(collection);
      // };
      // NFTMetadata();

      // Subscription Cleanup
      return () => {
        if (provider.removeListener) {
          provider.removeListener('accountsChanged', handleAccountsChanged);
          provider.removeListener('chainChanged', handleChainChanged);
          provider.removeListener('disconnect', handleDisconnect);
        }
      };
    }
  }, [provider, disconnect]);

  function getNetwork(chainId: number) {
    switch (chainId) {
      case 1:
        return 'Ethereum Mainnet';
      case 3:
        return 'Ethereum Ropsten';
      case 4:
        return 'Ethereum Rinkeby';
      case 5:
        return 'Ethereum Goerli';
      case 42:
        return 'Ethereum Kovan';
      case 56:
        return 'BSC Mainnet';
      case 97:
        return 'BSC Testnet';
      case 1337:
        return 'Local Hardhat Network';
      default:
        return 'Not Recognized';
    }
  }

  return (
    <div>
      {web3Provider ? (
        <div className={'flex flex-col items-center justify-center'}>
          <button
            className={
              'text-md font-bold uppercase px-6 py-2 w-auto mt-6 rounded block dark:text-green-500 bg-indigo-400 dark:bg-indigo-700'
            }
            onClick={disconnect}
          >
            {t('disconnectWallet')}
          </button>
          <p className={'mt-4'}>Address: {address}</p>
          <p>Network: {getNetwork(chainId)}</p>
          <p>ChainID: {chainId}</p>
        </div>
      ) : (
        <div>
          <button
            className={
              'text-md font-bold uppercase px-6 py-2 w-auto mt-6 rounded block dark:text-green-500 bg-indigo-400 dark:bg-indigo-700'
            }
            onClick={connect}
          >
            {t('connectWallet')}
          </button>
        </div>
      )}
    </div>
  );
};
