import React, { useState, useEffect } from 'react';
import { MobileView, BrowserView } from 'react-device-detect';
import paymentTypes from '@data/paymentTypes';
import PropTypes from 'prop-types';
const theme = require('tailwind.config').theme;

interface selCryptoButtonProps {
  color: string;
  tickerSelect: Function;
}
function SelCrypto({ color, tickerSelect }: selCryptoButtonProps) {
  const payments = paymentTypes();
  const tickers = [];
  for (const item of payments) {
    tickers.push(item.symbol);
  }

  let currWidth = theme.screens.xl.replace(/(\d+)(p[tx])$/, '$1');
  if (typeof window !== 'undefined') {
    currWidth = window.innerWidth.toString();
  }

  const [paymentType, setPaymentType] = useState(0);
  const breakWidth = Number.parseInt(
    theme.screens.xl.replace(/(\d+)(p[tx])$/, '$1'),
    10
  );
  const [width, setWidth] = useState(Number.parseInt(currWidth, 10));
  useEffect(() => {
    function handleWindowResize() {
      setWidth(window.innerWidth);
    }

    window.addEventListener('resize', handleWindowResize);
    tickerSelect(tickers[paymentType]);
    return () => window.removeEventListener('resize', handleWindowResize);
  }, [paymentType]);

  return (
    <>
      <ul className="flex flex-row max-w-xs pt-1 mb-0 overflow-x-scroll list-none md:overflow-x-auto sm:overflow-x-auto md:max-w-xl lg:max-w-2xl xl:max-w-4xl space-x-2">
        {payments.map((type, i) => (
          <li key={i} className="inline-flex mb-px last:mr-0">
            <a
              className={
                'text-xs font-bold uppercase px-5 py-1 shadow-lg rounded block leading-normal ' +
                (paymentType === i
                  ? 'text-white ' + color
                  : 'text-' + color + '-600 bg-indigo-400 dark:bg-indigo-900')
              }
              onClick={(event) => {
                event.preventDefault();
                setPaymentType(i);
              }}
              data-toggle="button"
              href={`#link${i}`}
              role="button"
              aria-pressed="false"
            >
              <i className="text-base ">
                <MobileView>{type.ticker}</MobileView>
                <BrowserView>
                  {width < breakWidth ? type.ticker : type.long}
                </BrowserView>
              </i>
            </a>
          </li>
        ))}
      </ul>
    </>
  );
}

SelCrypto.propTypes = {
  color: PropTypes.string.isRequired,
  tickerSelect: PropTypes.func.isRequired,
};

export default function SelCryptoButtonsRender(props: selCryptoButtonProps) {
  return (
    <SelCrypto color={props.color ? props.color : 'red'} {...props} />
  );
}
