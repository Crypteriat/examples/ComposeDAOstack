import React, { useState } from 'react';
import SelCrypto from '@components/SelCrypto';
import { Web3ModalComponent } from './Web3Modal';

export default function BuyWindow(props: { handleClose: React.MouseEventHandler<HTMLButtonElement>; }) {
  const [tickerSymbol, tickerSelect] = useState('btc');

  return (
    <div
      className={
        'overflow-auto h-2/5 w-5/12 p-6 top-1/3 border rounded-xl bg-gray-200 dark:bg-gray-800 fixed'
      }
    >
      <div className={'text-right -mt-4 mb-4'}>
        <button onClick={props.handleClose}>x</button>
      </div>
      <div className={'flex flex-col items-center'}>
        <b className={'text-xl text-center'}>How would you like to pay?</b>
        <div className={'mt-3'}>
          <SelCrypto
            tickerSelect={tickerSelect}
            color="bg-blue-400 dark:bg-blue-500"
          />
        </div>
        <p className={'text-center mt-4'}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <Web3ModalComponent></Web3ModalComponent>
      </div>
    </div>
  );
}
