/* eslint-disable indent */
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { AssetTransfer } from 'typechain-types/contracts/AssetTransfer';
import { AssetTransfer__factory } from 'typechain-types/factories/contracts/AssetTransfer__factory';
import { expect } from 'chai';
import { ethers } from 'hardhat';
// import { useTokenContract } from '../hooks';
// import { renderHook } from '@testing-library/react-hooks';
import { BigNumber } from 'ethers';

const ADDRESS_ZERO = ethers.constants.AddressZero;
const buyValueInWei = 2000000000000000000; // 2 Ether, 2000000000 Gwei

export function toETH(amount: string) {
  return ethers.utils.formatUnits(BigNumber.from(amount), 'ether');
}

describe('AssetTransfer Test', () => {
  let AssetTransfer: AssetTransfer;
  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addr4: SignerWithAddress;
  let addr5: SignerWithAddress;

  before('Deploys AssetTransfer and child contracts', async () => {
    [owner, addr1, addr2, addr3, addr4, addr5] = await ethers.getSigners();

    const AssetTransferFactory = (await ethers.getContractFactory(
      'contracts/AssetTransfer.sol:AssetTransfer',
      owner
    )) as unknown as AssetTransfer__factory;
    AssetTransfer = await AssetTransferFactory.deploy();
    await AssetTransfer.deployed();
    AssetTransfer.initialize();

    /*
    const { result } = renderHook(() => useTokenContract(tokenContractAdress)); // Need to fix custom hook (provider & signer)
    ERC1155Token = await result.current;
    */
  });

  it('Initializes owner & mints tokens', async () => {
    expect(await AssetTransfer.getOwner()).to.equal(owner.address);
    expect(
      (await AssetTransfer.balanceOf(owner.address, 10)).toString()
    ).to.equal('100000');
  });

  it('Initialize can only be called once', async () => {
    await expect(AssetTransfer.initialize()).to.be.revertedWith(
      'Initializable: contract is already initialized'
    );
  });

  // eslint-disable-next-line quotes
  it("Test 'get' functions", async () => {
    expect(await AssetTransfer.getTokenCount()).to.equal(0);
    expect(await AssetTransfer.getCurrentAssetPrice()).to.equal(0);
  });

  describe('Purchase', () => {
    it('Successful purchase', async () => {
      // Price 2 ether, TokenCount 1
      await AssetTransfer.connect(addr1).purchaseNFT({
        value: BigInt(buyValueInWei),
      });

      expect(await AssetTransfer.getTokenCount()).to.equal(1);
      expect(await AssetTransfer.getCurrentAssetPrice()).to.equal(
        BigInt(buyValueInWei)
      );

      expect(await AssetTransfer.balanceOf(addr1.address, 1)).to.equal(1);
      expect(await AssetTransfer.balanceOf(addr1.address, 2)).to.equal(0);

      expect(toETH((await owner.getBalance()).toString())).to.equal('10002.0');
      expect(toETH((await addr1.getBalance()).toString())).to.equal('9998.0');
    });

    it('TokenId count and price revert if failed purchase', async () => {
      await expect(AssetTransfer.connect(addr1).purchaseNFT({ value: 3 })).to.be
        .reverted;
      expect(await AssetTransfer.getTokenCount()).to.equal(1);
      expect(await AssetTransfer.getCurrentAssetPrice()).to.equal(
        BigInt(buyValueInWei)
      );
    });

    it('Reverts if insufficient funds', async () => {
      await expect(
        AssetTransfer.connect(addr1).purchaseNFT({ value: 0 })
      ).to.be.revertedWith('AssetTransfer: Not enough funds to purchase');
      await expect(
        AssetTransfer.connect(addr1).purchaseNFT({ value: 200 })
      ).to.be.revertedWith('AssetTransfer: Not enough funds to purchase');
    });

    it('Reverts if zero address', async () => {
      await expect(
        AssetTransfer.connect(ADDRESS_ZERO).purchaseNFT({ value: 1 })
      ).to.be.reverted;
    });

    it('Token balances match after purchase', async () => {
      // Price 4 ether, TokenCount 2
      await AssetTransfer.connect(addr1).purchaseNFT({
        value: BigInt(2 * buyValueInWei),
      });
      // Price 6 ether TokenCount 3
      await AssetTransfer.connect(addr2).purchaseNFT({
        value: BigInt(3 * buyValueInWei),
      });
      expect(await AssetTransfer.balanceOf(addr1.address, 2)).to.equal(1);
      const result = await AssetTransfer.balanceOfBatch(
        [addr2.address, addr2.address],
        [3, 2]
      );
      expect(result[0]).to.equal(1);
      expect(result[1]).to.equal(0);
    });

    it('Ether(wei) balances match after purchase', async () => {
      expect(toETH((await owner.getBalance()).toString())).to.equal('10012.0');
      expect(toETH((await addr1.getBalance()).toString())).to.equal('9994.0');
      expect(toETH((await addr2.getBalance()).toString())).to.equal('9994.0');
    });

    it('Price increases after purchase', async () => {
      const beforePrice = await AssetTransfer.getCurrentAssetPrice();
      // Price 8 ether, TokenCount 4
      await AssetTransfer.connect(addr3).purchaseNFT({
        value: BigInt(buyValueInWei * 4),
      });
      expect(await AssetTransfer.getCurrentAssetPrice()).to.gte(beforePrice);
      expect(await AssetTransfer.getCurrentAssetPrice()).to.equal(
        BigInt(buyValueInWei * 4)
      );
    });

    it('Returns excess funds (if any)', async () => {
      //Price 10 ether, sending 12 ether, TokenCount 5
      await AssetTransfer.connect(addr3).purchaseNFT({
        value: BigInt(buyValueInWei * 6),
      });
      expect(toETH((await addr3.getBalance()).toString())).to.equal('9982.0');
      expect(toETH((await owner.getBalance()).toString())).to.equal('10030.0');
    });

    it('Emits purchased and minted events', async () => {
      //Price 12 ether, TokenCount 6
      await expect(
        AssetTransfer.connect(addr4).purchaseNFT({
          value: BigInt(buyValueInWei * 6),
        })
      )
        .to.emit(AssetTransfer, 'PurchasedNFT')
        .withArgs(addr4.address, BigInt(buyValueInWei * 6));
      //Price 14 ether, TokenCount 7
      await expect(
        AssetTransfer.connect(addr4).purchaseNFT({
          value: BigInt(buyValueInWei * 7),
        })
      )
        .to.emit(AssetTransfer, 'MintedNFT')
        .withArgs(addr4.address, 7);
    });

    it('Pays royalty to correct recipient', async () => {
      //addr5 is royalty recipient
      await AssetTransfer.setRoyalty(8, addr5.address, 1000); //10%
      //Price 16 ether, sending 18, TokenCount 8
      await AssetTransfer.connect(addr4).purchaseNFT({
        value: BigInt(buyValueInWei * 9),
      });
      expect(toETH((await addr5.getBalance()).toString())).to.equal('10001.6');
      expect(toETH((await addr4.getBalance()).toString())).to.equal('9958.0');
      expect(toETH((await owner.getBalance()).toString())).to.equal('10070.4');
    });
  });

  describe('Purchase Batch', () => {});
});
