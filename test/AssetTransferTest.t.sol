// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.10;

import 'ds-test/test.sol';
import 'forge-std/Test.sol';
import 'forge-std/Vm.sol';
import '../contracts/AssetTransfer.sol';
import 'openzeppelin-contracts/contracts/token/ERC1155/utils/ERC1155Holder.sol';

contract AssetTransferTest is DSTest, ERC1155Holder {
  using stdStorage for StdStorage;

  Vm private vm = Vm(HEVM_ADDRESS);
  AssetTransfer private assetTransfer;
  StdStorage private stdstore;
  address public owner;

  event PurchasedNFT(address buyer, uint256 amount);
  event MintedNFT(address to, uint256 tokenId);

  function setUp() public {
    assetTransfer = new AssetTransfer();
    assetTransfer.initialize();

    owner = address(this);
  }

  function testInitializeTwice() public {
    vm.expectRevert('Initializable: contract is already initialized');
    assetTransfer.initialize();
  }

  function testInitializeOwner() public {
    assertEq(assetTransfer.getOwner(), owner);
    assertEq(assetTransfer.balanceOf(owner, 10), 100000);
  }

  function testGetTokenCount() public {
    assertEq(assetTransfer.getTokenCount(), 0);
    vm.expectRevert('AssetTransfer: Not enough funds to purchase');
    assetTransfer.purchaseNFT{value: 1 ether}();
    assertEq(assetTransfer.getTokenCount(), 0);
  }

  function testGetCurrentAssetPrice() public {
    assertEq(assetTransfer.getCurrentAssetPrice(), 0);
    vm.expectRevert('AssetTransfer: Not enough funds to purchase');
    assetTransfer.purchaseNFT{value: 1 ether}();
    assertEq(assetTransfer.getCurrentAssetPrice(), 0);
  }

  function testFailPurchaseToZeroAddress() public {
    vm.startPrank(address(0));
    vm.deal(address(0), 3 ether);
    assetTransfer.purchaseNFT{value: 3 ether}();
    vm.stopPrank();
  }

  function testFailNoPricePaid() public {
    assetTransfer.purchaseNFT();
  }

  function testInsufficientFunds() public {
    vm.expectRevert('AssetTransfer: Not enough funds to purchase');
    assetTransfer.purchaseNFT{value: 1 ether}();
  }

  function testSuccessfulPurchase() public {
    vm.startPrank(address(1));
    vm.deal(address(1), 3 ether);

    uint256 buyerBeginBalance = address(1).balance;
    uint256 ownerBeginBalance = owner.balance;
    assetTransfer.purchaseNFT{value: 3 ether}();

    assertEq(assetTransfer.getTokenCount(), 1);
    assertEq(assetTransfer.getCurrentAssetPrice(), 2 ether);

    assertEq(assetTransfer.balanceOf(address(1), 1), 1);
    assertEq(assetTransfer.balanceOf(address(1), 2), 0);

    assertEq(address(1).balance, buyerBeginBalance - 2 ether);
    assertEq(owner.balance, ownerBeginBalance + 2 ether);

    vm.stopPrank();
  }

  function testPriceIncreaseAfterPurchase() public {
    assertEq(assetTransfer.getCurrentAssetPrice(), 0);
    vm.prank(address(2));
    vm.deal(address(2), 2 ether);

    assetTransfer.purchaseNFT{value: 2 ether}();
    assertEq(assetTransfer.getCurrentAssetPrice(), 2 ether);
    assertEq(assetTransfer.getTokenCount(), 1);

    vm.prank(address(3));
    vm.deal(address(3), 4 ether);

    assetTransfer.purchaseNFT{value: 4 ether}();
    assertEq(assetTransfer.getCurrentAssetPrice(), 4 ether);
    assertEq(assetTransfer.getTokenCount(), 2);
  }

  function testReturnExcessFunds() public {
    vm.prank(address(1));
    vm.deal(address(1), 3.5 ether);

    assetTransfer.purchaseNFT{value: 3.5 ether}();
    assertEq(assetTransfer.getCurrentAssetPrice(), 2 ether);
    assertEq(address(1).balance, 1.5 ether);
  }

  function testPaysRoyalty() public {
    assetTransfer.setRoyalty(1, address(1), 1000); // 10%
    uint256 royaltyBeginBalance = address(1).balance;
    vm.prank(address(2));
    vm.deal(address(2), 3 ether);

    assetTransfer.purchaseNFT{value: 2 ether}();
    assertEq(address(1).balance, royaltyBeginBalance + 200000000000000000);
  }

  function testEmittedEvents() public {
    vm.expectEmit(true, true, false, false);
    emit PurchasedNFT(address(1), 2 ether);
    emit MintedNFT(address(1), 1);
    vm.prank(address(1));
    vm.deal(address(1), 2 ether);
    assetTransfer.purchaseNFT{value: 2 ether}();
  }

  fallback() external payable {}

  receive() external payable {}
}
