import { expect } from 'chai';
import { ethers } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ERC2981Royalties__factory } from 'typechain-types/factories/contracts/ERC2981Royalties__factory';
import { ERC2981Royalties } from 'typechain-types/contracts/ERC2981Royalties';

const ADDRESS_ZERO = ethers.constants.AddressZero;

describe('Royalties Test', async () => {
  let RoyaltiesContract: ERC2981Royalties;
  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;

  before('Deploys', async function () {
    [owner, addr1, addr2, addr3] = await ethers.getSigners();

    const RoyaltiesContractFactory = (await ethers.getContractFactory(
      'contracts/ERC2981Royalties.sol:ERC2981Royalties',
      owner
    )) as unknown as ERC2981Royalties__factory;
    RoyaltiesContract = await RoyaltiesContractFactory.deploy();
    await RoyaltiesContract.deployed();
    RoyaltiesContract.initialize();
  });

  it('Initialize can only be called once', async () => {
    await expect(RoyaltiesContract.initialize()).to.be.revertedWith(
      'Initializable: contract is already initialized'
    );
  });

  it('Only owner can set token royalty', async function () {
    await expect(
      RoyaltiesContract.connect(addr1).setTokenRoyalty(1, addr1.address, 500)
    ).to.be.reverted;
  });

  it('Sets correct royalty info', async function () {
    await RoyaltiesContract.connect(owner).setTokenRoyalty(
      1,
      addr1.address,
      500
    ); //5%
    const result = await RoyaltiesContract.royaltyInfo(1, 100);
    expect(result[0]).to.equal(addr1.address);
    expect(result[1].toNumber()).to.equal(5);

    const result2 = await RoyaltiesContract.royaltyInfo(2, 100);
    expect(result2[0]).to.equal(ADDRESS_ZERO);
    expect(result2[1].toNumber()).to.equal(0);
  });

  it('Can set royalty to 0', async function () {
    await RoyaltiesContract.connect(owner).setTokenRoyalty(1, addr1.address, 0);
    const result = await RoyaltiesContract.royaltyInfo(1, 100);
    expect(result[0]).to.equal(addr1.address);
    expect(result[1].toNumber()).to.equal(0);
  });

  it('Requires royalty value to be <= 10000, 100%', async function () {
    await expect(
      RoyaltiesContract.connect(owner).setTokenRoyalty(1, addr1.address, 10001) //100.1%
    ).to.be.revertedWith('ERC2981Royalties: Too high');
  });

  it('Reverts if recipient is zero address', async () => {
    await expect(
      RoyaltiesContract.connect(owner).setTokenRoyalty(1, ADDRESS_ZERO, 0)
    ).to.be.revertedWith('ERC2981Royalties: Recipient cannot be 0');
  });

  describe('Batch', () => {
    it('Requires array lengths to match', async () => {
      await expect(
        RoyaltiesContract.connect(owner).setTokenRoyaltyBatch(
          [1, 2],
          [addr1.address, addr2.address],
          [500]
        )
      ).to.be.revertedWith('Arrays length mismatch');
      await expect(
        RoyaltiesContract.connect(owner).setTokenRoyaltyBatch(
          [1, 2],
          [addr1.address],
          [500, 1000]
        )
      ).to.be.revertedWith('Arrays length mismatch');
    });

    it('Sets correct royalty info', async () => {
      await RoyaltiesContract.connect(owner).setTokenRoyaltyBatch(
        [1, 2, 3],
        [addr1.address, addr2.address, addr3.address],
        [500, 1000, 1500] //5%, 10%, 15%
      );
      const result1 = await RoyaltiesContract.royaltyInfo(1, 100);
      const result2 = await RoyaltiesContract.royaltyInfo(2, 100);
      const result3 = await RoyaltiesContract.royaltyInfo(3, 100);

      expect(result1[0]).to.equal(addr1.address);
      expect(result1[1].toNumber()).to.equal(5);

      expect(result2[0]).to.equal(addr2.address);
      expect(result2[1].toNumber()).to.equal(10);

      expect(result3[0]).to.equal(addr3.address);
      expect(result3[1].toNumber()).to.equal(15);
    });
  });
});
