import { expect } from 'chai';
import { ethers } from 'hardhat';
import {
  PriceFeedETH,
  PriceFeedETH__factory,
  PriceFeed,
  PriceFeed__factory,
} from '../typechain-types';
import { chainlink } from '../data/contractAddresses';

describe('PriceFeed Test', () => {
  let priceFeedETH: PriceFeedETH;
  let priceFeed: PriceFeed;
  const feedRegistryAddress = chainlink.Mainnet;
  const stmxFeedAddress = chainlink.stmxfeed;
  const ethFeedAddress = chainlink.ethfeed;

  before('Deploys', async function () {
    //Contract: PriceFeedETH
    const accountsETH = await ethers.getSigners();
    const signerETH = accountsETH[0];
    const priceFeedFactoryETH = (await ethers.getContractFactory(
      'contracts/PriceFeed.sol:PriceFeedETH',
      signerETH
    )) as unknown as PriceFeedETH__factory;
    priceFeedETH = await priceFeedFactoryETH.deploy(feedRegistryAddress);
    await priceFeedETH.deployed();

    //Contract: PriceFeedSTMX
    const accounts = await ethers.getSigners();
    const signer = accounts[0];
    const priceFeedFactory = (await ethers.getContractFactory(
      'contracts/PriceFeed.sol:PriceFeed',
      signer
    )) as unknown as PriceFeed__factory;
    priceFeed = await priceFeedFactory.deploy();
    await priceFeedETH.deployed();
  });

  // Testing Price Feed Registry
  // it('Should return ETH in USD', async function () {
  //   const price = await priceFeedETH.getETHUSDPrice();
  //   expect(price).gte('0');
  //   const decimals = await priceFeedETH.getETHDecimals();
  //   console.log('decimals: %s', decimals.toString());
  //   const usdPrice = price.toNumber() / Math.pow(10, decimals);
  //   // const usdPrice = parseFloat(BigNumber(price).div( 10 ** decimals) ).toFixed(decimals);
  //   console.log('ETH Price is: $ %s', usdPrice.toString());
  // });

  // Testing Price Feed
  it('Should return ETH in USD', async function () {
    const price = await priceFeed.getPrice(ethFeedAddress);
    expect(price).gte('0');
    const decimals = await priceFeed.getDecimals(ethFeedAddress);
    const usdPrice = price.toNumber() / Math.pow(10, decimals);
    console.log('ETH Price is: $ %s', usdPrice.toString());
  });

  it('Should return STMX in USD', async function () {
    const price = await priceFeed.getPrice(stmxFeedAddress);
    expect(price).gte('0');
    const decimals = await priceFeed.getDecimals(stmxFeedAddress);
    const usdPrice = price.toNumber() / Math.pow(10, decimals);
    console.log('ETH Price is: $ %s', usdPrice.toString());
  });

  // it('Should return STMX in ETH', async function () {});
});
