import { expect } from 'chai';
import {
  createDir,
  getHash,
  removeFileOrDir,
  listDir,
  getStats,
  publishToIPNS,
  writeMetadata,
} from '../utils/IPFSFunctions';

describe('IPFS Test', function () {
  let baseURL: string;
  let URI: string;
  let metadataDirHash: string;
  let metadataIPNS;

  before('Creates metadata & assets folders', async function () {
    await createDir('/Metadata');
    await createDir('/Assets');
    metadataDirHash = await getHash('/Metadata');

    this.timeout(100000);
    metadataIPNS = await publishToIPNS(metadataDirHash);

    baseURL = `https://gateway.ipfs.io/ipns/${metadataIPNS.name}/`;
    URI = `${baseURL}{id}.json`;
    // console.log('Base URL:', baseURL);
  });

  after('Removes assets & metadata folders', async function () {
    await removeFileOrDir('/Metadata');
    await removeFileOrDir('/Assets');
  });

  describe('URI and IPFS', async () => {
    it('Uploads folders to IPFS', async function () {
      await getStats('/Metadata');
      await getStats('/Assets');
    });

    it('Uploads image and metadata to IPFS', async function () {
      const images = ['Newport_Beach_View.png', 'scarface.jpeg'];

      await writeMetadata(1, 'My first NFT', 'This is only a test', images[0]);
      await writeMetadata(
        2,
        'My second NFT',
        'This is only another test',
        images[1]
      );

      let count = 1;
      for await (const file of await listDir('/Metadata')) {
        let countToString = count.toString();
        countToString = countToString.padStart(64, '0');
        expect(`${countToString}.json`).to.eq(file.name);
        expect(await getHash(`/Metadata/${countToString}.json`)).to.equal(
          file.cid.toString()
        );
        count++;
      }

      count = 0;
      for await (const file of await listDir('/Assets')) {
        expect(images[count]).to.eq(file.name);
        expect(await getHash(`/Assets/${images[count]}`)).to.eq(
          file.cid.toString()
        );
        count++;
      }
    });
  });
});
