# ComposeDAO Full dApp Framework

Built with [Foundry](https://github.com/foundry-rs/foundry), [Hardhat](https://hardhat.org/), [React](https://reactjs.org/), and [TailwindCSS](https://tailwindcss.com/)

## Required Steps for Testing and Deployment

Install Foundry -> https://book.getfoundry.sh/getting-started/installation.html

```
npm i # install project npm modules
forge install # install project foundry modules
npm i -g hardhat-shorthand solhint solhint-plugin-prettier # install shorthand prettier, and linting tools
hardhat-completion install # installs autocompletions for hh commands
alias hh='TS_NODE_COMPILER_OPTIONS={\"module\":\"commonjs\"} hh' # required in .bashrc, etc to properly setup TS compiler. NextJS uses esnext, so hardhat commands need commonjs compiler option set for usage
brew install ipfs # if on Mac; else follow instructions: ipfs://bafybeiextwtmxzaepqmx27hvoxj5oyhe5ye73kcrpxur56nrmqabscwpma/install/command-line/#official-distributions
ipfs init
```

Configure `.env` file

This framework has been written to use locahost and the various test networks. Test ETH has been encoded into the scripts for localhost deployment. For testnets, test ETH or BNB is required which can be received from their corresponding faucets. An .env is used for running on other networks outside of localhost.

## Development Items & Process

- Define Top Level Smart Contract Structure

* UniqueToken
* IERC 721
* Migration

- Design:

* Use ERC721 OpenZeppelin contract, use IERC721 mint, counter, and X functions

## npm commands

NOTE: Windows can not interpret `;` in npm scripts so run scripts accordingly when using Windows machine

NOTE: check/update const VARIABLES defined at top of each file. Some pulled from data/nftConfig.ts

```
npm run build # compiles smart contracts and runs production build
npm run compile # compiles smart contracts
npm run clean # cleans hardhat cache
npm run start # starts dapp in development mode
npm run dev # starts dapp in development mode with local hardhat node
npm run serve # serves a production build
```

NOTE: check/update const VARIABLES defined at top of each file. Some pulled from data/nftConfig.ts <br>
`npm run uploadnftmetadata` - two functions in file (nft/UploadNftMetadata.ts) - Configure main() to run specific functions

- **UploadNftMetadata** (uploads both json dir and image dir configured at top of file to IPFS via NFT.Storage)
- **UploadJsonDir** (uploads just json dir to IPFS via NFT.Storage)

`npm run metadatacheck` - three functions in file (nft/MetadataCheck.ts) - Configure main() to run specific functions

- **flagGoldIds** (goes through json dir to flag token ids with gold wheel chair attribute. outputs to defined file)
- **matchGoldIds** (pulls gold ids from LOADRECORD and checks if in gold id list, returns array of ids not found in gold id list)
- **swapMetadata** (swaps the metadata of token ids not found in gold id list (result from matchGoldIds) with metadata in gold id list, starting from the bottom(end))

`npm run verifybalance` - two functions in file (scripts/verifyBalance.ts) - Configure main() to run specific functions

- **verifyTokenBalances** (verifies token balances from load file against blockchain. outputs to defined file)
- **findTokenOwner** (attempts to find the owner of tokenId against addresses from loadFile) INCOMPLETE

```
npm run startipfs # starts local ipfs node
npm run stopipfs # stops local ipfs node
npm run test # runs unit tests via mocha/hardhat and jest
npm run test:solidity # runs unit tests via mocha/hardhat
npm run test:forge # runs tests via forge (tests smart contracts in contracts/ dir)
npm run test:hooks # runs hooks unit tests via mocha/hardhat
npm run test:ipfs # starts local ipfs node; runs ipfs mocha tests; stops local ipfs node
npm run test:ipfs-alone # only runs mocha ipfs tests. Use for Windows
npm run node # starts locally forked evm with contract from mainnet
npm run gas # runs tests and provides gas usage
```

```
(Configue smart contract deployment in scripts/instantiate.ts)
npm run deploy:localhost # deploys smart contract to local node
npm run deploy:rinkeby # deploys smart contract to rinkeby testnet
npm run deploy:bsctestnet # deploys smart contract to bsc testnet
npm run deploy:mainnet # deploys smart contract to bsc mainnt
```

`npm run interact:bsctestnet` bsc testnet <br>
`npm run interact:bscmainnet` bsc mainnet <br>
`npm run interact:rinkby` eth testnet

- Run functions defined in scripts/interact.ts

NOTE: check/update const VARIABLES defined at top of each file. Some pulled from data/nftConfig.ts <br>
`npm run mint:bsctestnet`<br>
`npm run mint:bscmainnt`

- Mints token ids from load_file to corresponding addresses, will do batch minting if possible. Removes completed lines from load_file so make sure to make copy. Outputs transactions to defined file

`npm run seturi:bsctestnet` <br>
`npm run seturi:bsctmainnet`

- calls setUri function with specfied uri.

```
npm run verify:bsctestnet <deployed_contract_address> # verifies smart contract on bscscan testnet
npm run verify:bscmainnet <deployed_contract_address> # verifies smart contract on bscscan
npm run verify:rinkeby <deployed_contract_address> # verifies smart contract on etherscan
```

## Hard Hat Tasks (standard plus additional added)

```
hh accounts # lists local test addresses/accounts
hh compile # compiles code and produces typings
hh clean # cleans compilations/typings
hh test  # runs unit tests preceed with `REPORT_GAS=true or REPORT_SIZE=true` for gas and size reports on compile/test
hh web3 # tests alchemy api calls
hh watch node # watches for file changes and redeploys to local blockchain
hh deploy --network <network> # choices are localhost, rospinet, bsctestnet, bscmainnet
hh local # runs React app on localhost, interacting with hard hat implementation
hh price --contract <contract address> # gets latest price from Chainlink Price Feed, addresses found here: https://docs.chain.link/docs/ethereum-addresses/
hh size-contracts # sizes contracts
```
