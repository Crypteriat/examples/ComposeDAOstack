import fs from 'fs';

export async function removeLineFromFile(
  filePath: string,
  email: string,
  id: number,
  address: string
) {
  fs.readFile(filePath, { encoding: 'utf-8' }, function (err, data) {
    if (err) throw new Error('Error' + err);

    const dataArray = data.split('\n'); // convert file data in an array
    const searchEmail = email; // we are looking for a line, contains, key word in the file
    const searchId = id.toString();
    const searchAddress = address;
    let lastIndex = -1; // let say, we have not found the keyword

    for (let index = 0; index < dataArray.length; index++) {
      if (
        dataArray[index].includes(searchEmail) &&
        dataArray[index].includes(searchId) &&
        dataArray[index].includes(searchAddress)
      ) {
        // check if a line contains the keyword
        lastIndex = index; // found a line includes a keyword
        break;
      }
    }

    dataArray.splice(lastIndex, 1); // remove the keyword from the data Array

    // UPDATE FILE WITH NEW DATA
    const updatedData = dataArray.join('\n');
    fs.writeFile(filePath, updatedData, (err) => {
      if (err) throw err;
      // console.log('Successfully updated the file data');
    });
  });
}

export function getLoadFileData(filePath: string) {
  const fileDataArray = fs.readFileSync(filePath).toString().split('\n');

  const idArray: number[] = [];
  const addressArray: string[] = [];
  const emailsArray: string[] = [];
  const isGoldArray: string[] = [];

  // Start at line 1, not 0, for column Names
  for (let i = 1; i < fileDataArray.length; i++) {
    // Breack each line into an array separated by a comma ,
    const currDataLine = fileDataArray[i].split(',');
    const currEmail = currDataLine[1];
    const currTokenId = currDataLine[3];
    const currAddress = currDataLine[4];
    const currIsGold = currDataLine[5];

    idArray.push(parseInt(currTokenId));
    addressArray.push(currAddress);
    emailsArray.push(currEmail);
    isGoldArray.push(currIsGold);
  }

  return {
    tokenIds: idArray,
    addresses: addressArray,
    emails: emailsArray,
    isGolds: isGoldArray,
  };
}

export function getGoldIdFileData(filePath: string) {
  const fileDataArray = fs.readFileSync(filePath).toString().split('\n');

  const goldIdArray: number[] = [];

  // Start at line 1, not 0, for column Names
  for (let i = 1; i < fileDataArray.length; i++) {
    const currDataLine = fileDataArray[i].split(',');
    if (currDataLine[0]) {
      const currGoldId = parseInt(currDataLine[0]);
      goldIdArray.push(currGoldId);
    }
  }

  return goldIdArray;
}
