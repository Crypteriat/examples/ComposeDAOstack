import { ethers } from 'hardhat';
import { BigNumber, Contract } from 'ethers';

export async function mintBatch(
  contract: Contract,
  toAddress: string,
  tokenIDS: Array<number>,
  tokenAmounts: Array<number>
) {
  //check array length 'tokenIDS' & 'tokenAmounts' match and every value in 'tokenAmounts' is 1
  if (
    tokenIDS.length === tokenAmounts.length &&
    tokenAmounts.every((v) => v === 1)
  ) {
    const transaction = await contract.mintBatch(
      toAddress,
      tokenIDS,
      tokenAmounts,
      []
    );
    return transaction;
  } else {
    throw new Error(
      'Error batch minting. Check tokenIDS and tokenAmounts array lengths and values.'
    );
  }
}

export async function transferBatch(
  contract: Contract,
  fromAddress: string,
  toAddress: string,
  tokenIDS: Array<number>,
  tokenAmounts: Array<number>
) {
  //check array length 'tokenIDS' & 'tokenAmounts' match and every value in 'tokenAmounts' is 1
  if (
    tokenIDS.length === tokenAmounts.length &&
    tokenAmounts.every((v) => v === 1)
  ) {
    const transaction = await contract.safeBatchTransferFrom(
      fromAddress,
      toAddress,
      tokenIDS,
      tokenAmounts,
      []
    );
    return transaction;
  } else {
    throw new Error(
      'Error batch transferring. Check tokenIDS and tokenAmounts array lengths and values.'
    );
  }
}

export async function balanceOfBatch(
  contract: Contract,
  addressArr: string[],
  tokenIds: number[]
) {
  //check array length 'tokenIds' & 'address'
  if (tokenIds.length === addressArr.length) {
    const balances: BigNumber[] = await contract.balanceOfBatch(
      addressArr,
      tokenIds
    );
    return balances;
  } else {
    throw new Error(
      'Error running balanceOfBatch. Check tokenIds and addressArr array lengths.'
    );
  }
}

export async function verifyBscTxn(txHash: string, chainId: number) {
  if (chainId !== 97 && chainId !== 56) {
    throw new Error(
      'invalid chainId provided: ' + chainId + '. Options are 97 and 56 for BSC'
    );
  }

  const bscProvider = new ethers.providers.JsonRpcProvider(
    chainId === 56
      ? 'https://bsc-dataseed.binance.org/'
      : 'https://data-seed-prebsc-1-s1.binance.org:8545',
    { name: 'binance', chainId: chainId }
  );
  const txReceipt = await bscProvider.getTransactionReceipt(txHash);

  if (txReceipt && txReceipt.blockNumber) {
    // console.log(txReceipt);
    return true;
  }

  // console.log(txReceipt);
  return false;
}
