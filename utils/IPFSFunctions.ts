import { create } from 'ipfs-http-client';
import fs from 'fs';

const ipfs = create();

export async function publishToIPNS(addr: string) {
  const result = await ipfs.name.publish(addr);
  // You now receive a res which contains two fields:
  //   - name: the name under which the content was published.
  //   - value: the "real" address to which Name points.
  return result;
}

export async function resolveIPNSName(addr: string) {
  // The IPNS address you want to resolve.
  for await (const name of ipfs.name.resolve(addr)) {
    console.log(name);
  }
}

export async function createDir(path: string) {
  await ipfs.files.mkdir(path, {
    parents: true,
    cidVersion: 1,
    hashAlg: 'sha2-256',
  });
}

export async function writeFile(path: string, content: any) {
  await ipfs.files.write(path, content, {
    create: true,
    cidVersion: 1,
    hashAlg: 'sha2-256',
  });
}

export async function removeFileOrDir(path: string) {
  await ipfs.files.rm(path, {
    recursive: true,
    cidVersion: 1,
    hashAlg: 'sha2-256',
  });
}

export async function getHash(path: string) {
  const result = await ipfs.files.stat(path, {
    hash: true,
  });

  return result.cid.toString();
}

export async function listDir(path: string) {
  return await ipfs.files.ls(path);
}

export async function getStats(path: string) {
  return await ipfs.files.stat(path);
}

export const metadata_template = {
  name: '',
  description: '',
  image: '',
  properties: {},
};

let NFTMetadataToString: string;

export async function writeMetadata(
  tokenId: number,
  name: string,
  description: string,
  imageName: string
) {
  const NFTMetadata = metadata_template;
  const baseURL = 'https://ipfs.io/ipfs/';

  let tokenIdString = tokenId.toString();
  tokenIdString = tokenIdString.padStart(64, '0');

  NFTMetadata.name = name;
  NFTMetadata.description = description;

  const image = Buffer.from(fs.readFileSync(`./img/${imageName}`));
  await writeFile(`/Assets/${imageName}`, image);
  const imageHash = await getHash(`/Assets/${imageName}`);
  NFTMetadata.image = baseURL + imageHash;

  NFTMetadataToString = JSON.stringify(NFTMetadata, null, 2);
  await writeFile(`/Metadata/${tokenIdString}.json`, NFTMetadataToString);
}
