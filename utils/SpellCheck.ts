import fs from 'fs';

type NFTJsonData = {
  image: string;
  name: string;
  description: string;
  attributes: AttributeValueTraits[];
};

type AttributeValueTraits = {
  value: string;
  trait_type: string;
};

// Path to directory and file for spell check
const DIR_PATH = './nft/data/json/';
const FILE_PATH = './nft/data/json/0000_ALL.json';

export async function SpellCheck(dirPath: string, filePath: string) {
  const dirFiles = await fs.promises.readdir(dirPath);

  for (let i: number = 1; i <= dirFiles.length; i++) {
    //get the number for image and convert to regular base 10 string
    let strVal = i.toString();
    console.log('processing image id ' + strVal);
    //get the related json for the image
    let jsonFile = JSON.parse(
      fs.readFileSync(dirPath + strVal + '.json').toString()
    );
    //update json with ipfs location of image and any other data
    //console.log(jsonFile.attributes)
    jsonFile.description = 'TOAX FOUNDING NFT COLLECTION';
    for (let i = 0; i < jsonFile.attributes.length; i++) {
      let trait: AttributeValueTraits = jsonFile.attributes[i];
      if (trait.value === 'Balack') {
        trait.value = 'Black';
      }
      if (trait.value === 'Deanim') {
        trait.value = 'Denim';
      }
      if (trait.value === 'SupperMan') {
        trait.value = 'SuperMan';
      }
      if (trait.value === 'Patch Deanim') {
        trait.value = 'Patch Denim';
      }
      if (trait.value === 'Balack') {
        trait.value = 'Black';
      }
      if (trait.value === 'Basic Character ') {
        trait.value = 'Basic Character';
      }
      if (trait.trait_type === 'Weel Chair') {
        trait.trait_type = 'Wheel Chair';
      }
      //console.log("type: " + trait.trait_type + " value " + trait.value);
      jsonFile.attributes[i] = trait;
    }
    fs.writeFileSync(dirPath + strVal + '.json', JSON.stringify(jsonFile));
    console.log('finished image ' + strVal);
  }

  /////////////////////////////////////////////////////////////////////
  //modifying the giant file

  let jsonFile = JSON.parse(fs.readFileSync(filePath).toString());

  for (let i: number = 0; i < 10000; i++) {
    let strVal = i.toString();

    let obj: NFTJsonData = jsonFile[i];
    console.log('processing image id ' + strVal);
    // console.log('name ' + obj.name);
    // console.log('img ' + obj.image);
    // console.log('desc ' + obj.description);
    obj.description = 'TOAX FOUNDING NFT COLLECTION';
    for (let j: number = 0; j < obj.attributes.length; j++) {
      let trait: AttributeValueTraits = obj.attributes[j];
      //   console.log('trait ' + trait.trait_type);
      //   console.log('value' + trait.value);
      if (trait.value === 'Balack') {
        trait.value = 'Black';
      }
      if (trait.value === 'Deanim') {
        trait.value = 'Denim';
      }
      if (trait.value === 'SupperMan') {
        trait.value = 'SuperMan';
      }
      if (trait.value === 'Patch Deanim') {
        trait.value = 'Patch Denim';
      }
      if (trait.value === 'Balack') {
        trait.value = 'Black';
      }
      if (trait.value === 'Basic Character ') {
        trait.value = 'Basic Character';
      }
      if (trait.trait_type === 'Weel Chair') {
        trait.trait_type = 'Wheel Chair';
      }
      obj.attributes[j] = trait;
    }
    jsonFile[i] = obj;
    console.log('finished image ' + strVal);
  }

  fs.writeFileSync(filePath, JSON.stringify(jsonFile));
}

SpellCheck(DIR_PATH, FILE_PATH).catch((err) => {
  console.error(err);
  process.exit(1);
});
