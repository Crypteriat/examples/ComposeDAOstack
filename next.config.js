const { i18n } = require('./next-i18next.config')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = withBundleAnalyzer({
  i18n,
  reactStrictMode: true,
  trailingSlash: true,
  pageExtensions: ['js', 'jsx', 'md', 'mdx', 'ts', 'tsx'],
  future: {},
  images: {
    domains: ['ipfs.io'],
  },
  webpack: (config, { dev, isServer }) => {
    config.module.rules.push(
      {
        test: /\.(png|jpe?g|gif|mp4)$/i,
        exclude: [/.*test.ts/, /node_modules/],
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '/_next',
              name: 'static/media/[name].[hash].[ext]',
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    )

    // if (!dev && !isServer) {
    //   // Replace React with Preact only in client production build
    //   Object.assign(config.resolve.alias, {
    //     react: 'preact/compat',
    //     'react-dom/test-utils': 'preact/test-utils',
    //     'react-dom': 'preact/compat',
    //   });
    // }

    return config
  },
  /*
  async rewrites() {
    return {
      beforeFiles: [
        // These rewrites are checked after headers/redirects
        // and before pages/public files which allows overriding
        // page files
        {
          source: '/',
          destination: '/index',
        },
      ],
    };
  },*/
  // async redirects() {
  //   return [
  //     {
  //       source: '/post',
  //       destination: '/meetings',
  //       permanent: true,
  //     },
  //   ];
  // },
})
