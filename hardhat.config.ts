import fs from 'fs';
import { task } from 'hardhat/config';
import '@nomiclabs/hardhat-waffle';
import '@nomiclabs/hardhat-ethers';
import '@nomiclabs/hardhat-etherscan';
import 'hardhat-deploy';
import '@typechain/hardhat';
import 'solidity-coverage';
import 'hardhat-watcher';
import 'hardhat-contract-sizer';
import 'hardhat-gas-reporter';
import 'hardhat-abi-exporter';
import 'hardhat-change-network';
import 'hardhat-preprocessor';
import {
  ALCHEMY_API_KEY,
  ALCHEMY_BLOCK,
  ROPSTEN_PRIVATE_KEY,
  KOVAN_PRIVATE_KEY,
  GOERLI_PRIVATE_KEY,
  RINKEBY_PRIVATE_KEY,
  BNB_TESTNET_PRIVATE_KEY,
  BNB_MAINNET_PRIVATE_KEY,
  ETHERSCAN_API_KEY,
  BSCSCAN_API_KEY,
} from './endpoints.config';

task('accounts', 'Prints the list of accounts', async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

task('balances', 'Prints the list of accounts', async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    const balance = await account.getBalance();
    console.log(account.address + ' : ' + balance.toString() + ' gwei');
  }
});

function getRemappings() {
  return fs
    .readFileSync('remappings.txt', 'utf8')
    .split('\n')
    .filter(Boolean)
    .map((line) => line.trim().split('='));
}

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  // localNetworksConfig: './networks.ts',
  solidity: {
    compilers: [
      {
        version: '0.8.10',
      },
    ],
  },
  defaultNetwork: 'hardhat',
  networks: {
    hardhat: {
      gasPrice: 0,
      initialBaseFeePerGas: 0,
      allowUnlimitedContractSize: true,
      chainId: 1337,
      forking: {
        url: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
        blockNumber: parseInt(ALCHEMY_BLOCK),
      },
    },
    localhost: {
      gasPrice: 0,
    },
    bscTestnet: {
      url: 'https://data-seed-prebsc-1-s1.binance.org:8545',
      chainId: 97,
      accounts: [BNB_TESTNET_PRIVATE_KEY],
    },
    bsc: {
      url: 'https://bsc-dataseed.binance.org/',
      chainId: 56,
      accounts: [BNB_MAINNET_PRIVATE_KEY],
    },
    goerli: {
      url: `https://eth-goerli.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      chainId: 5,
      accounts: [GOERLI_PRIVATE_KEY],
    },
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      chainId: 4,
      accounts: [RINKEBY_PRIVATE_KEY],
    },
    ropsten: {
      url: `https://eth-ropsten.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      chainId: 3,
      accounts: [`0x${ROPSTEN_PRIVATE_KEY}`],
    },
    kovan: {
      url: `https://eth-kovan.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      chainId: 42,
      accounts: [`0x${KOVAN_PRIVATE_KEY}`],
    },
  },
  etherscan: {
    apiKey: {
      rinkeby: `${ETHERSCAN_API_KEY}`,
      bscTestnet: `${BSCSCAN_API_KEY}`,
      bsc: `${BSCSCAN_API_KEY}`,
    },
  },
  watcher: {
    compilation: {
      tasks: ['compile'],
    },
    node: {
      tasks: [
        'compile',
        { command: 'compile', params: { quiet: true } },
        'node',
        { command: 'node', params: { noCompile: true } },
      ],
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS ? true : false,
  },
  contractSizer: {
    alphaSort: true,
    runOnCompile: process.env.REPORT_SIZE ? true : false,
    disambiguatePaths: false,
  },
  abiExporter: {
    path: './hooks/utils/abi/',
    runOnCompile: true,
    clear: true,
    only: ['PriceFeed', 'ERC1155Token', 'AssetTransfer', 'ERC2981Royalties'],
    spacing: 2,
  },
  preprocess: {
    eachLine: (hre) => ({
      transform: (line: string) => {
        if (line.match(/^\s*import /i)) {
          getRemappings().forEach(([find, replace]) => {
            if (line.match(find)) {
              line = line.replace(find, replace);
            }
          });
        }
        return line;
      },
    }),
  },
};
