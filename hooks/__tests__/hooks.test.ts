import { expect } from 'chai';
import hre, { ethers } from 'hardhat';
import {
  useTokenContract,
  useAssetTransferContract,
  useRoyaltiesContract,
  usePriceFeedContract,
} from '../';
import { beforeAll, jest } from '@jest/globals';
import { renderHook } from '@testing-library/react';
import { PriceFeed } from 'typechain-types/contracts/PriceFeed.sol';
import { chainlink } from '../../data/contractAddresses';
import { ERC1155Token } from 'typechain-types/contracts/ERC1155Token';
import { AssetTransfer } from 'typechain-types/contracts/AssetTransfer';

describe('Hooks', function () {
  let assetTransferContract: AssetTransfer;
  let tokenContract: ERC1155Token;
  let priceFeedContract: PriceFeed;
  jest.setTimeout(30000);
  const ethFeedAddress = chainlink.ethfeed;

  beforeAll(async () => {
    await hre.run('compile');
    hre.changeNetwork('localhost');

    // AssetTransfer Contract \\
    const ASSETTRANSFER_CONTRACT = await ethers.getContractFactory(
      'AssetTransfer'
    );
    assetTransferContract = await ASSETTRANSFER_CONTRACT.deploy();

    // Token Contract \\
    const TOKEN_CONTRACT = await ethers.getContractFactory('ERC1155Token');
    tokenContract = await TOKEN_CONTRACT.deploy();

    await tokenContract.deployed();

    // Price Feed Contract \\
    const PRICE_FEED_CONTRACT = await ethers.getContractFactory('PriceFeed');
    priceFeedContract = await PRICE_FEED_CONTRACT.deploy();

    await priceFeedContract.deployed();
  });

  it('Uses useAssetTransferContract to access contract', async function () {});

  it('Uses useTokenContract to access contract', async function () {
    const { result } = renderHook(() =>
      useTokenContract(tokenContract.address)
    );
    const tokenContractHook = await result.current;

    const maxSupply = await tokenContractHook.getMaxSupply();
    const tokenCount = await tokenContractHook.getTokenCount();
    expect(maxSupply).to.equal('500');
    expect(tokenCount).to.equal('0');
  });

  it('Uses useRoyaltiesContract to access contract', async function () {});

  it('Uses usePriceFeedContract to access contract', async function () {
    const { result } = renderHook(() =>
      usePriceFeedContract(priceFeedContract.address)
    );
    const priceFeedContractHook = await result.current;

    const price = await priceFeedContract.getPrice(ethFeedAddress);
    expect(price).gte('0');
    const decimals = await priceFeedContractHook.getDecimals(ethFeedAddress);
    const usdPrice = price.toNumber() / Math.pow(10, decimals);
    console.log('ETH Price is: $ %s', usdPrice.toString());
  });
});
