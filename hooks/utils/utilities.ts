import { ethers, ContractInterface } from 'ethers';

import PriceFeed_ABI from '../utils/abi/contracts/PriceFeed.sol/PriceFeed.json';
import ERC1155Token_ABI from '../utils/abi/contracts/ERC1155Token.sol/ERC1155Token.json';
import ERC2981Royalties_ABI from '../utils/abi/contracts/ERC2981Royalties.sol/ERC2981Royalties.json';
import AssetTransfer_ABI from '../utils/abi/contracts/AssetTransfer.sol/AssetTransfer.json';

export const ERROR_CODES = [
  'INVALID_AMOUNT',
  'INVALID_TRADE',
  'INSUFFICIENT_ETH_GAS',
  'INSUFFICIENT_SELECTED_TOKEN_BALANCE',
  'INSUFFICIENT_ALLOWANCE',
];

export function isAddress(addr: string) {
  return ethers.utils.isAddress(addr);
}

//signer is optional
export function getProviderOrSigner(provider, signer) {
  return signer ? signer : provider;
}

// account is optional
export function getContract(
  tokenAddress,
  ABI: ContractInterface,
  provider,
  signer
) {
  if (
    !isAddress(tokenAddress) ||
    tokenAddress === ethers.constants.AddressZero
  ) {
    throw Error(`Invalid 'address' parameter '${tokenAddress}'.`);
  }

  return new ethers.Contract(
    tokenAddress,
    ABI,
    getProviderOrSigner(provider, signer)
  );
}

export function getTokenContract(tokenAddress, provider, signer) {
  return getContract(tokenAddress, ERC1155Token_ABI, provider, signer);
}

export function getRoyaltiesContract(tokenAddress, provider, signer) {
  return getContract(tokenAddress, ERC2981Royalties_ABI, provider, signer);
}

export function getAssetTransferContract(tokenAddress, provider, signer) {
  return getContract(tokenAddress, AssetTransfer_ABI, provider, signer);
}

export function getPriceFeedContract(contractAddress, provider, signer) {
  return getContract(contractAddress, PriceFeed_ABI, provider, signer);
}
