import { useMemo } from 'react';
import { ethers } from 'ethers';
import providerContext from '../context/ProviderContext';
import {
  getTokenContract,
  getPriceFeedContract,
  getRoyaltiesContract,
  getAssetTransferContract,
} from './utils/utilities';

export async function useAssetTransferContract(
  tokenAddress: string,
  withSignerIfPossible = true
) {
  const web3modal = providerContext.getModal();
  let provider;
  let signer;
  if (web3modal) {
    provider = await web3modal.connect();
    signer = await providerContext.getSigner();
  } else {
    provider = new ethers.providers.JsonRpcProvider();
    signer = provider.getSigner();
  }

  return useMemo(() => {
    return getAssetTransferContract(
      tokenAddress,
      provider,
      withSignerIfPossible ? signer : undefined
    );
  }, [provider, signer, tokenAddress, withSignerIfPossible]);
}

export async function useTokenContract(
  tokenAddress: string,
  withSignerIfPossible = true
) {
  const web3modal = providerContext.getModal();
  let provider;
  let signer;
  if (web3modal) {
    provider = await web3modal.connect();
    signer = await providerContext.getSigner();
  } else {
    provider = new ethers.providers.JsonRpcProvider();
    signer = provider.getSigner();
  }

  return useMemo(() => {
    return getTokenContract(
      tokenAddress,
      provider,
      withSignerIfPossible ? signer : undefined
    );
  }, [provider, signer, tokenAddress, withSignerIfPossible]);
}

export async function useRoyaltiesContract(
  tokenAddress: string,
  withSignerIfPossible = true
) {
  const provider = new ethers.providers.JsonRpcProvider();
  const signer = provider.getSigner();

  return useMemo(() => {
    return getRoyaltiesContract(
      tokenAddress,
      provider,
      withSignerIfPossible ? signer : undefined
    );
  }, [provider, signer, tokenAddress, withSignerIfPossible]);
}

export async function usePriceFeedContract(
  tokenAddress: string,
  withSignerIfPossible = true
) {
  const provider = new ethers.providers.JsonRpcProvider();
  const signer = provider.getSigner();

  return useMemo(() => {
    return getPriceFeedContract(
      tokenAddress,
      provider,
      withSignerIfPossible ? signer : undefined
    );
  }, [provider, signer, tokenAddress, withSignerIfPossible]);
}
