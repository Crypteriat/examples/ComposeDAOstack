/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  verbose: true,
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: [
    '<rootDir>/**/__tests__/*.ts',
    '<rootDir>/**/__tests__/*.test.ts',
  ],
  testPathIgnorePatterns: ['test/', 'node_modules/'],
}
