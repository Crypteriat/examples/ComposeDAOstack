module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:@next/next/recommended',
    'react-app',
  ],
  env: {
    browser: true,
    amd: true,
    node: true,
    jest: true,
  },
  globals: {
    document: true,
    _: true,
    $: true,
    window: true,
  },
  rules: {
    'jsx-quotes': ['error', 'prefer-double'],
    quotes: ['warn', 'single'],
    // indent: ['warn', 2],
    'react-hooks/exhaustive-deps': 0,
    'react/react-in-jsx-scope': 'off',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['hrefLeft', 'hrefRight'],
        aspects: ['invalidHref', 'preferButton'],
      },
    ],
    'react/prop-types': 0,
    'no-unused-vars': 0,
    'react/no-unescaped-entities': 0,
    'react/function-component-definition': 0,
  },
};
