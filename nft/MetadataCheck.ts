import fs from 'fs';
import 'dotenv/config';
import nftConfig from '../data/nftConfig';
import { getLoadFileData, getGoldIdFileData } from '../utils/FileData';

const NFT_OUTPUT_FILE = './nft/data/gold_ids.csv';
const NFT_DATA_FILE = nftConfig.nftLoadFile;
const GOLD_IDS_FILE = './nft/data/gold_ids.csv';
const IMAGE_DIR = './nft/data/images/';
const METADATA_DIR = './nft/data/PROD4/';

type AttributeValueTraits = {
  value: string;
  trait_type: string;
};

// reads through json directory and flags token ids with gold attribute
async function flagGoldIds(imageDirectory: string, nftDirectory: string) {
  const imageFiles = await fs.promises.readdir(imageDirectory);
  const nftFiles = await fs.promises.readdir(nftDirectory);

  var stream = fs.createWriteStream(NFT_OUTPUT_FILE, {
    flags: 'a',
  });
  stream.write('ID, HEX_ID \n');

  let totalGold = 0;
  //make sure directory lengths are the same
  if (nftFiles.length === imageFiles.length) {
    console.log(
      `Checking json directory: ${nftDirectory} and outputting Gold Ids to: ${NFT_OUTPUT_FILE}`
    );

    //hard code 10000 to test all files there
    for (let i = 1; i <= 10000; i++) {
      //get the number for image in hex and pad it as necessary for nft standards
      const id = i.toString(16);
      const tokenId = id.padStart(64, '0');

      //get the related json file
      try {
        const jsonFile = JSON.parse(
          fs.readFileSync(`${nftDirectory}${tokenId}.json`).toString()
        );

        for (let j = 0; j < jsonFile.attributes.length; j++) {
          let trait: AttributeValueTraits = jsonFile.attributes[j];
          if (trait.value === 'Gold' && trait.trait_type === 'Wheel Chair') {
            totalGold = totalGold + 1;
            const entryData = i + ',' + id;
            stream.write(entryData + '\n');
          }
        }
      } catch (e: unknown) {
        console.log('missing id: ' + i);
        console.log('tokenId: ' + tokenId);
      }
    }

    console.log('totalGold: ' + totalGold);
  } else {
    throw new Error('Image and Metadata directory lengths do not match.');
  }
}

// Pulls gold ids from LOADRECORD and checks if in gold id list
// returns array of ids not found in gold id list
async function matchGoldIds(loadFile: string, goldIdsFile: string) {
  const loadFileData = getLoadFileData(loadFile);
  const goldIds = getGoldIdFileData(goldIdsFile);
  const isGolds = loadFileData.isGolds;
  const tokenIds = loadFileData.tokenIds;

  const goldIdsFromLoad: number[] = [];
  const flaggedGoldIds: number[] = [];

  let goldCount = 0;
  if (isGolds.length === tokenIds.length) {
    for (let i = 0; i < isGolds.length; i++) {
      if (isGolds[i] === 'GOLD') {
        const currTokenId: number = tokenIds[i];
        goldIdsFromLoad.push(currTokenId);
        goldCount++;
      }
    }

    console.log(
      `Found ${goldCount} tokenIds marked as gold in: ${NFT_DATA_FILE}`
    );
    await new Promise((f) => setTimeout(f, 2000));

    let flaggedCount = 0;
    if (goldIdsFromLoad.length === goldCount) {
      for (let i = 0; i < goldIdsFromLoad.length; i++) {
        if (!goldIds.includes(goldIdsFromLoad[i])) {
          console.log(
            `FLAGGED! Did not find Token ID: ${goldIdsFromLoad[i]} in Gold IDs list.`
          );
          flaggedCount++;
          flaggedGoldIds.push(goldIdsFromLoad[i]);
        }
      }

      console.log(`Total flagged count: ${flaggedCount}`);
    } else {
      throw new Error('goldIdsFromLoad length does not equal goldCount');
    }
  } else {
    throw new Error('isGoldsLength and tokenIdsLength do not match.');
  }

  return flaggedGoldIds;
}

// swaps the metadata of token ids not found in gold id list (result from matchGoldIds)
// with metadata in gold id list, starting from the bottom(end)
async function swapMetadata(
  loadFile: string,
  goldIdsFile: string,
  metadataDir: string
) {
  const goldIds = getGoldIdFileData(goldIdsFile);
  const notGoldIds = await matchGoldIds(loadFile, goldIdsFile);

  let lastGoldId = goldIds.length - 1;
  for (let i = 0; i < notGoldIds.length; i++) {
    // get token Ids in hex
    const goldIdHex = goldIds[lastGoldId].toString(16).padStart(64, '0');
    const notGoldIdHex = notGoldIds[i].toString(16).padStart(64, '0');

    // get corresponding json metadata before swap
    let goldIdJson = JSON.parse(
      fs.readFileSync(`${metadataDir}${goldIdHex}.json`).toString()
    );
    let notgGoldIdJson = JSON.parse(
      fs.readFileSync(`${metadataDir}${notGoldIdHex}.json`).toString()
    );

    // make the swap
    const tempJson = notgGoldIdJson;
    notgGoldIdJson = goldIdJson;
    goldIdJson = tempJson;

    // re-write the files back to directory
    fs.writeFileSync(
      `${metadataDir}${notGoldIdHex}.json`,
      JSON.stringify(notgGoldIdJson)
    );

    fs.writeFileSync(
      `${metadataDir}${goldIdHex}.json`,
      JSON.stringify(goldIdJson)
    );

    lastGoldId--;
  }
}

async function main() {
  await flagGoldIds(IMAGE_DIR, METADATA_DIR);
  await new Promise((f) => setTimeout(f, 2000));
  await matchGoldIds(NFT_DATA_FILE, GOLD_IDS_FILE);

  // swapMetadata(NFT_DATA_FILE, GOLD_IDS_FILE, METADATA_DIR);
}

main();
