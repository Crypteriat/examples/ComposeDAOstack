import { NFTStorage, File } from 'nft.storage';
import mime from 'mime';
import fs from 'fs';
import path from 'path';
import 'dotenv/config';
import { filesFromPath } from 'files-from-path';

const NFT_STORAGE_KEY = process.env.NFT_STORAGE_API;
const IMAGE_DIR = './nft/data/images/';
const JSON_DIR = './nft/data/PROD4/';

/**
 * A helper to read a file from a location on disk and return a File object.
 * Note that this reads the entire file into memory and should not be used for
 * very large files.
 * @param {string} filePath the path to a file to store
 * @returns {File} a File object containing the file content
 */
async function fileFromPath(filePath) {
  const content = await fs.promises.readFile(filePath);
  const type = mime.getType(filePath);
  return new File([content], path.basename(filePath), { type });
}

// This code uploads all files from provided 'image' and 'json' directories to IPFS via nft.storage
// Outputs json with updated image property in PROD directory './nft/data/PROD/'
async function UploadNftMetadata(
  imageDirectory: string,
  metadataDirectory: string
) {
  const nftstorage = new NFTStorage({ token: NFT_STORAGE_KEY });

  const imageFiles = await fs.promises.readdir(imageDirectory);
  const metadataFiles = await fs.promises.readdir(metadataDirectory);
  // console.log(files.length);

  //make sure directory lengths are the same
  if (metadataFiles.length === imageFiles.length) {
    for (let i = 1; i <= imageFiles.length; i++) {
      //get file, upload it and store the cid
      const imgFile = await fileFromPath(`${imageDirectory}${i}.png`);
      const imgCID = await nftstorage.storeBlob(imgFile);
      console.log('Processing image: ' + i + '.png, IPFS image CID: ' + imgCID);

      //get the number for image in hex and pad it as necessary for nft standards
      const id = i.toString(16);
      const tokenId = id.padStart(64, '0');
      console.log('Updating metadata for token Id: ' + i);

      //get the related json for the image
      const jsonFile = JSON.parse(
        fs.readFileSync(`${metadataDirectory}${i}.json`).toString()
      );

      //update json with ipfs location of image and any other data
      jsonFile.image = `ipfs://${imgCID}`;

      //store the new data in the json directory with correct hex value for name
      fs.writeFileSync(
        `./nft/data/PROD/${tokenId}.json`,
        JSON.stringify(jsonFile)
      );
    }

    //Upload all metadata files in output directory to IPFS
    const prodPath = './nft/data/PROD/';
    const files = filesFromPath(prodPath, {
      pathPrefix: path.resolve(prodPath),
      hidden: true, // use the default of false if you want to ignore files that start with '.'
    });

    console.log(`storing file(s) from ${prodPath}`);
    const directoryCid = await nftstorage.storeDirectory(files);
    console.log({ directoryCid });

    const status = await nftstorage.status(directoryCid);
    console.log(status);

    console.log(`Directory located at: ipfs://${directoryCid}`);
  } else {
    throw new Error('Image and Metadata directory lengths do not match.');
  }
}

async function UploadJsonDir(metadataDirectory: string) {
  const nftstorage = new NFTStorage({ token: NFT_STORAGE_KEY });

  const files = filesFromPath(metadataDirectory, {
    pathPrefix: path.resolve(metadataDirectory),
    hidden: true, // use the default of false if you want to ignore files that start with '.'
  });

  console.log(`storing file(s) from ${metadataDirectory}`);
  const directoryCid = await nftstorage.storeDirectory(files);
  console.log({ directoryCid });

  const status = await nftstorage.status(directoryCid);
  console.log(status);

  console.log(`Directory located at: ipfs://${directoryCid}`);
}

async function main() {
  // await UploadNftMetadata(IMAGE_DIR, JSON_DIR);
  // await UploadJsonDir(JSON_DIR);
}

main();
