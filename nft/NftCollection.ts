import { Network, initializeAlchemy, getNftsForOwner } from '@alch/alchemy-sdk';
// import { ALCHEMY_API_KEY, MORALIS_API_KEY } from '../endpoints.config';
import fetch from 'node-fetch';

export type NFTMetadata = {
  name: string;
  description: string;
  image: string;
  attributes: { object: object };
  contractAddress: string;
  tokenID: number;
  contractStandard?: string;
};

export async function getNftCollectionBSC(
  userAddress: string,
  contractAddress: string,
  chainID: number
) {
  function getNetwork(chainId: number) {
    switch (chainId) {
      case 1:
        // ETH MAINNET
        return '0x1';
      case 5:
        // ETH GOERLI
        return '0x5';
      case 56:
        // BSC MAINNET
        return '0x38';
      case 97:
        //BSC TESTNET
        return '0x61';
      default:
        return '0x61';
    }
  }
  const apiKey = process.env.NEXT_PUBLIC_MORALIS_API_KEY || '';
  const NFT_COLLECTION: NFTMetadata[] = [];

  const moralisFetch = await fetch(
    `https://deep-index.moralis.io/api/v2/${userAddress}/nft/${contractAddress}?chain=${getNetwork(
      chainID
    )}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': apiKey,
      },
    }
  );

  const moralisResponse: any = await moralisFetch.json();
  const nfts = moralisResponse.result;

  if (nfts) {
    // const nftCount = moralisResponse.total;

    for (let i = 0; i < nfts.length; i++) {
      const contractAddress = nfts[i].token_address;
      const tokenID = Number(nfts[i].token_id);
      const tokenType = nfts[i].contract_type;

      // if metadata is type object then nothing returned (null),
      // if type string then metadata returned, don't ask me why
      const tokenMetadata =
        typeof nfts[i].metadata === 'object'
          ? {
              name: 'metadata not found',
              description: 'metadata not found',
              image: '',
              attributes: {},
            }
          : JSON.parse(nfts[i].metadata);

      const metadata = {
        name: tokenMetadata.name,
        description: tokenMetadata.description,
        image: tokenMetadata.image.replace('ipfs://', 'https://ipfs.io/ipfs/'),
        attributes: tokenMetadata.attributes,
        contractAddress: contractAddress,
        tokenID: tokenID,
        contractStandard: tokenType,
      };

      NFT_COLLECTION.push(metadata);
    }
  }

  return NFT_COLLECTION;
}

async function getNftCollectionETH(userAddress: string, chainID: number) {
  function getNetwork(chainId: number) {
    switch (chainId) {
      case 1:
        return Network.ETH_MAINNET;
      case 3:
        return Network.ETH_ROPSTEN;
      case 4:
        return Network.ETH_RINKEBY;
      case 5:
        return Network.ETH_GOERLI;
      case 42:
        return Network.ETH_KOVAN;
      default:
        return Network.ETH_RINKEBY;
    }
  }

  const settings = {
    apiKey: process.env.NEXT_PUBLIC_ALCHEMY_API_KEY,
    // apiKey: ALCHEMY_API_KEY,
    network: getNetwork(chainID),
    maxRetries: 10,
  };
  const alchemy = initializeAlchemy(settings);

  const NFT_COLLECTION = [];
  const nfts = await getNftsForOwner(alchemy, userAddress);

  for (let i = 0; i < nfts.ownedNfts.length; i++) {
    const contractAddress = nfts.ownedNfts[i].contract.address;
    const tokenID = Number(nfts.ownedNfts[i].tokenId);
    const tokenURI = nfts.ownedNfts[i].tokenUri.raw;
    const tokenType = nfts.ownedNfts[i].tokenType;

    const tokenIDHex = tokenID.toString(16).padStart(64, '0');
    const tokenMetadataURI = tokenURI.replace('{id}', tokenIDHex);

    const tokenMetadataFetch = await fetch(
      tokenMetadataURI.replace('ipfs://', 'https://ipfs.io/ipfs/'),
      {
        method: 'GET',
      }
    );
    const tokenMetadata = await tokenMetadataFetch.json();

    const metadata = {
      name: tokenMetadata.name,
      description: tokenMetadata.description,
      image: tokenMetadata.image.replace('ipfs://', 'https://ipfs.io/ipfs/'),
      attributes: JSON.stringify(tokenMetadata.attributes),
      contractAddress: contractAddress,
      tokenID: tokenID,
      tokenType: tokenType,
    };

    NFT_COLLECTION.push(metadata);
  }

  return NFT_COLLECTION;
}

export async function getNftCollection(
  userAddress: string,
  contractAddress: string,
  chainID: number
) {
  if (chainID === 56 || chainID === 97) {
    const collection = await getNftCollectionBSC(
      userAddress,
      contractAddress,
      chainID
    );
    return collection;
  } else {
    const collection = await getNftCollectionETH(userAddress, chainID);
    return collection;
  }
}
