const addresses = {
  chainlink: {
    Mainnet:	'0x47Fb2585D2C56Fe188D0E6ec628a38b74fCeeeDf',
    Kovan:	'0xAa7F6f7f507457a1EE157fE97F6c7DB2BEec5cD0',
    stmxfeed:  '0x00a773bD2cE922F866BB43ab876009fb959d7C29',
    ethfeed: '0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419',
  }
}
export const chainlink = addresses.chainlink;
export default addresses;
