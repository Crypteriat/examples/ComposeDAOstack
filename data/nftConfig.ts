const nftConfig = {
  bscOwnerAddr: '0xDA5E82136EAAadf713192D4b7146705Dd170C637',
  bscAltAddr: '0xDc3B05Fa65ED46d786Ed20A5F473982ABAA59065',
  ethOwnerAddr: '0x2e0ba2c720CA9e4939E85Ef6351A78a8c71448f6',
  ethAltAddr: '0x951d5a6E12325eDf1C9F73b0ebC27C89A1cd1F01',
  bscTestnetContractAddr: '0x4A514b3da4A9E01a7f22d57e0a4b99bb0E367308',
  bscContractAddr: '0x42cC279462C31398650444aaC6d9410c8Ac7C62B',
  goerliContractAddr: '0x575C6921FbE7e9213ACfA1b6eaEAA81337bEa810',
  rinkebyContractAddr: '0x21d9f06025e7dFB7bA46dFb188Cb56c20D13982E',
  ethContractAddr: '',
  chainId: 97, // 56 for BSC mainnet & 97 for BSC testnet
  nftLoadFile: './nft/data/Test_LOADRECORD.csv',
  nftOutputFile: './nft/data/Test_SentNFTs.txt',
};

export default nftConfig;
