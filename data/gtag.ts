// Log the pageview with their URL
declare global {
    interface Window { gtag: any; }
}
export const pageview = (url: string) => {
  if (window) {
    window.gtag('config', process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS, {
      page_path: url, // eslint-disable-line camelcase
    });
  }
};

// Log specific events happening.
export const event = ({action, params}) => {
  window.gtag('event', action, params);
};
