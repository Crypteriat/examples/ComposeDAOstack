import CryptoIcon from '@components/crypto-icons';

export default function paymentTypes() {
  const ticker = ['BTC', 'LBTC', 'ETH', 'BNB', 'PAYPAL'];
  const longName = {
    btc: (
      <span className="flex flex-col items-center my-3 text-center">
        Bitcoin
      </span>
    ),
    lbtc: (
      <span className="flex flex-col items-center text-center">
        Lightning <br /> Bitcoin
      </span>
    ),
    eth: (
      <span className="flex flex-col items-center my-3 text-center">
        Ethereum
      </span>
    ),
    bnb: (
      <span className="flex flex-col items-center text-center">
        Binance <br /> Coin{' '}
      </span>
    ),
    paypal: (
      <span className="flex flex-col items-center my-3 text-center">
        PayPal
      </span>
    ),
  };
  let payments = [];

  ticker.forEach((item, i) => {
    payments.push({
      symbol: ticker[i],
      ticker: (
        <span className="flex flex-col items-center">
          <CryptoIcon kind={item} size={5} />
          {item}
        </span>
      ),
      long: (
        <span className="flex flex-col items-center">
          <CryptoIcon kind={item} size={5} />
          {longName[item.toLowerCase()]}
        </span>
      ),
    });
  });
  return payments;
}

