import WalletConnectProvider from '@walletconnect/web3-provider';
import Fortmatic from 'fortmatic';
import TrezorProvider from '@web3modal/trezor-provider';
import WalletLink from 'walletlink';

const ALCHEMY_API_KEY = process.env.NEXT_PUBLIC_ALCHEMY_API_KEY;
const FORTMATIC_LIVE_API_KEY = process.env.NEXT_PUBLIC_FORTMATIC_LIVE_API_KEY;
// const FORTMATIC_TEST_API_KEY = process.env.NEXT_PUBLIC_FORTMATIC_TEST_API_KEY;

export const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider, // required \\
    options: {
      rpc: {
        1: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
        3: `https://eth-ropsten.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
        42: `https://eth-kovan.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
        // ...
      },
    },
  },
  fortmatic: {
    package: Fortmatic, // required
    options: {
      key: FORTMATIC_LIVE_API_KEY, // required
    },
  },
  // 'custom-trezor': {
  //   display: {
  //     logo: '/img/TrezorLogo.png',
  //     name: 'Trezor',
  //     description: 'Connect to your Trezor account',
  //   },
  //   package: TrezorProvider,
  //   options: {
  //     apiKey: 'EXAMPLE_PROVIDER_API_KEY',
  //   },
  //   connector: async (ProviderPackage, options) => {
  //     const provider = new ProviderPackage(options);

  //     await provider.enable();

  //     return provider;
  //   },
  // },
  'custom-walletlink': {
    display: {
      logo: 'https://play-lh.googleusercontent.com/PjoJoG27miSglVBXoXrxBSLveV6e3EeBPpNY55aiUUBM9Q1RCETKCOqdOkX2ZydqVf0',
      name: 'Coinbase',
      description: 'Connect to Coinbase Wallet (not Coinbase App)',
    },
    options: {
      appName: 'Coinbase', // Your app name
      networkUrl: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      chainId: 1,
    },
    package: WalletLink,
    connector: async (options: {
      appName: string;
      networkUrl: string;
      chainId: number;
    }) => {
      const { appName, networkUrl, chainId } = options;
      const walletLink = new WalletLink({
        appName,
      });
      const provider = walletLink.makeWeb3Provider(networkUrl, chainId);
      await provider.enable();
      return provider;
    },
  },
};
