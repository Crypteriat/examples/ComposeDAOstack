module.exports = {
  title: 'NFT Purchase Demo',
  author: 'Crypteriat, Inc.',
  headerTitle: 'DeX Demo',
  description: 'Blockchains for the World',
  siteUrl: 'https://shakticrypto.com',
  siteRepo: 'https://gitlab.com/crypteriat/examples/NFTdemo',
  image: '/static/img/avatar.png',
  logo: '/static/img/shakti.jpg',
  socialBanner: '/static/img/twitter-card.png',
  email: '5647553-jlt-crypteriat@users.noreply.gitlab.com',
  gitlab: 'https://gitlab.com/Crypteriat/examples/NFTdemo',
  twitter: 'https://twitter.com/realCrypteriat',
  linkedin: 'https://www.linkedin.com/in/Crypteriat',
  locales: [{ en: 'English' }, { fr: 'français' }],
  language: 'English',
}
