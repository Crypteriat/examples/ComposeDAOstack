//SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.4;

import 'forge-std/console.sol';
import 'chainlink-brownie-contracts/contracts/src/v0.8/interfaces/FeedRegistryInterface.sol';
import 'chainlink-brownie-contracts/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol';
import 'chainlink-brownie-contracts/contracts/src/v0.8/Denominations.sol';

interface InterfacePriceFeed {
  function getPrice(address _priceFeed) external view returns (int256);

  function getDecimals(address _priceFeed) external view returns (uint8);
}

contract PriceFeedETH {
  FeedRegistryInterface internal registry;

  // address STMX = 0xbE9375C6a420D2eEB258962efB95551A5b722803; //STMX token contract address

  /**
   * Network: Ethereum Kovan
   * Feed Registry: 0xAa7F6f7f507457a1EE157fE97F6c7DB2BEec5cD0
   */
  constructor(address _registry) {
    registry = FeedRegistryInterface(_registry);
  }

  /**
   * Returns the ETH / USD price
   */
  function getETHUSDPrice() public view returns (int256) {
    (
      uint80 _roundID,
      int256 price,
      uint256 _startedAt,
      uint256 _timeStamp,
      uint80 _answeredInRound
    ) = registry.latestRoundData(Denominations.ETH, Denominations.USD);
    console.log('roundID: %s', _roundID, ' startedAt: %s', _startedAt);
    console.log(
      ' timeStamp: %s',
      _timeStamp,
      ' answeredInRound: %s',
      _answeredInRound
    );
    // console.log('ETH decimals: ');
    // console.log(registry.decimals(Denominations.ETH, Denominations.USD));
    return price;
  }

  function getETHDecimals() public view returns (uint8) {
    return registry.decimals(Denominations.ETH, Denominations.USD);
  }

  /**
   * Returns the latest price
   */

  function getPrice(address base, address quote) public view returns (int256) {
    (
      uint80 _roundID,
      int256 price,
      uint256 _startedAt,
      uint256 _timeStamp,
      uint80 _answeredInRound
    ) = registry.latestRoundData(base, quote);
    console.log('roundID: %s', _roundID, ' startedAt: %s', _startedAt);
    console.log(
      ' timeStamp: %s',
      _timeStamp,
      ' answeredInRound: %s',
      _answeredInRound
    );
    return price;
  }
}

contract PriceFeed {
  // AggregatorV3Interface internal defaultPriceFeed;

  // mapping(address => AggregatorV3Interface) public Feeds;
  // mapping(address => uint8) public Decimals;

  /**
   * Network: Mainnet
   * Aggregator: STMX/USD
   * Address: 0x00a773bD2cE922F866BB43ab876009fb959d7C29
   */
  // constructor(address _priceFeed) {
  //   defaultPriceFeed = AggregatorV3Interface(_priceFeed); //Default STMX-USD
  //   // Feeds[_priceFeed] = defaultPriceFeed;
  // }

  // function getDecimals(address _priceFeed) public returns (uint8) {
  //   AggregatorV3Interface priceFeed = AggregatorV3Interface(_priceFeed);
  //   if (Feeds[priceFeed] == 0) {
  //     Feeds[priceFeed] = priceFeed.decimals();
  //     return priceFeed.decimals();
  //   } else {
  //     return priceFeed.decimals();
  //   }
  // }

  /**
   * Returns latest price of parameter priceFeed
   */
  function getPrice(address _priceFeed) external view returns (int256) {
    AggregatorV3Interface priceFeed = AggregatorV3Interface(_priceFeed);
    (
      uint80 _roundID,
      int256 price,
      uint256 _startedAt,
      uint256 timeStamp,
      uint80 _answeredInRound
    ) = priceFeed.latestRoundData();

    console.log('roundID: %s', _roundID);
    console.log('startedAt: %s', _startedAt);
    console.log('answeredInRound: %s', _answeredInRound);
    // If the round is not complete yet, timestamp is 0
    require(timeStamp > 0, 'Round not complete');
    // console.log('STMX decimals: %s', defaultPriceFeed.decimals());
    return price;
  }

  function getDecimals(address _priceFeed) external view returns (uint8) {
    AggregatorV3Interface priceFeed = AggregatorV3Interface(_priceFeed);
    return priceFeed.decimals();
  }
}
