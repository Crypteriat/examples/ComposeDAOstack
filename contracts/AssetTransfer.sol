//SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import 'openzeppelin-contracts-upgradeable/contracts/proxy/utils/Initializable.sol';
import 'openzeppelin-contracts/contracts/utils/Counters.sol';
import 'openzeppelin-contracts/contracts/utils/math/SafeMath.sol';
import 'openzeppelin-contracts/contracts/token/ERC1155/utils/ERC1155Holder.sol';

import 'contracts/ERC1155Token.sol';
import 'contracts/ERC2981Royalties.sol';

contract AssetTransfer is Initializable, ERC1155Holder {
  using Counters for Counters.Counter;
  using SafeMath for uint256;

  Counters.Counter private _tokenIdCounter;
  address payable private _owner;

  ERC1155Token private _tokenContract;
  ERC2981Royalties private _royaltyContract;

  event PurchasedNFT(address buyer, uint256 amount);
  event MintedNFT(address to, uint256 tokenId);

  function initialize() public initializer {
    _tokenContract = new ERC1155Token();
    _tokenContract.initialize();
    _royaltyContract = new ERC2981Royalties();
    _royaltyContract.initialize();

    _owner = payable(msg.sender);
    _tokenContract.mint(_owner, 10, 10**5, '');
  }

  function getOwner() public view returns (address) {
    return _owner;
  }

  function balanceOf(address addr, uint256 id) public view returns (uint256) {
    return _tokenContract.balanceOf(addr, id);
  }

  function balanceOfBatch(address[] memory addrs, uint256[] memory ids)
    public
    view
    returns (uint256[] memory)
  {
    return _tokenContract.balanceOfBatch(addrs, ids);
  }

  function getTokenCount() public view returns (uint256) {
    return _tokenIdCounter.current();
  }

  function getCurrentAssetPrice() public view returns (uint256) {
    return (_tokenIdCounter.current() * 2000000000000000000); // 2 Ether
  }

  function setRoyalty(
    uint256 id,
    address recipient,
    uint256 value
  ) public {
    _royaltyContract.setTokenRoyalty(id, recipient, value);
  }

  function purchaseNFT() external payable {
    _tokenIdCounter.increment();
    uint256 tokenId = _tokenIdCounter.current();
    uint256 price = getCurrentAssetPrice();

    require(msg.value >= price, 'AssetTransfer: Not enough funds to purchase');

    uint256 change = msg.value.sub(price);
    (address receiver, uint256 royaltyAmount) = _royaltyContract.royaltyInfo(
      tokenId,
      price
    );

    (bool sentOwner, ) = _owner.call{
      value: (msg.value.sub(change)).sub(royaltyAmount)
    }('');
    require(sentOwner, 'To owner: Failed to send Ether');

    (bool sentReceiver, ) = receiver.call{value: royaltyAmount}('');
    require(sentReceiver, 'To royalty recipient: Failed to send Ether');

    (bool sentMsgSender, ) = msg.sender.call{value: change}('');
    require(sentMsgSender, 'To msg.sender: Failed to send Ether');

    emit PurchasedNFT(msg.sender, msg.value);
    _tokenContract.mint(msg.sender, tokenId, 1, '');
    emit MintedNFT(msg.sender, tokenId);
  }
}
