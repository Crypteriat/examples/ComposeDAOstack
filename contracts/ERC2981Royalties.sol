// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import 'openzeppelin-contracts/contracts/utils/introspection/ERC165.sol';
import 'openzeppelin-contracts-upgradeable/contracts/access/OwnableUpgradeable.sol';
import 'openzeppelin-contracts-upgradeable/contracts/proxy/utils/Initializable.sol';
import './interfaces/IERC2981Royalties.sol';

contract ERC2981Royalties is
  Initializable,
  ERC165,
  IERC2981Royalties,
  OwnableUpgradeable
{
  struct Royalty {
    address recipient;
    uint256 value;
  }
  mapping(uint256 => Royalty) internal _royalties;

  function initialize() public initializer {
    __Ownable_init();
  }

  function supportsInterface(bytes4 interfaceId)
    public
    view
    virtual
    override
    returns (bool)
  {
    return
      interfaceId == type(IERC2981Royalties).interfaceId ||
      super.supportsInterface(interfaceId);
  }

  /*
    @dev Sets token royalties
    @param id the token id for which we register the royalties
    @param recipient recipient of the royalties
    @param value percentage (using 2 decimals - 10000 = 100, 0 = 0)
  */
  function setTokenRoyalty(
    uint256 id,
    address recipient,
    uint256 value
  ) public onlyOwner {
    require(value <= 10000, 'ERC2981Royalties: Too high');
    require(recipient != address(0), 'ERC2981Royalties: Recipient cannot be 0');

    _royalties[id] = Royalty(recipient, value);
  }

  /*
    @notice Mint several tokens at once
    @param ids array of ids of the token types to mint
    @param royaltyRecipients an array of recipients for royalties (if royaltyValues[i] > 0)
    @param royaltyValues an array of royalties asked for (EIP2981)
  */
  function setTokenRoyaltyBatch(
    uint256[] memory ids,
    address[] memory royaltyRecipients,
    uint256[] memory royaltyValues
  ) external onlyOwner {
    require(
      ids.length == royaltyRecipients.length &&
        ids.length == royaltyValues.length,
      'ERC2981Royalties: Arrays length mismatch'
    );

    for (uint256 i; i < ids.length; i++) {
      setTokenRoyalty(ids[i], royaltyRecipients[i], royaltyValues[i]);
    }
  }

  /*
    @notice Called with the sale price to determine how much royalty
      is owed and to whom.
    @param _tokenId - the NFT asset queried for royalty information
    @param _value - the sale price of the NFT asset specified by _tokenId
    @return _receiver - address of who should be sent the royalty payment
    @return _royaltyAmount - the royalty payment amount for value sale price
  */
  function royaltyInfo(uint256 tokenId, uint256 value)
    external
    view
    override
    returns (address receiver, uint256 royaltyAmount)
  {
    Royalty memory royalty = _royalties[tokenId];
    return (royalty.recipient, (value * royalty.value) / 10000);
  }
}
