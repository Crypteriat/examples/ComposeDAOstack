import { ethers } from 'hardhat';
import nftConfig from '../data/nftConfig';

const BSC_CONTRACT_ADDRESS = nftConfig.bscTestnetContractAddr;
const URI =
  'ipfs://bafybeihdpjn7hjhxb325hf3kv4mmppadv3rh4yuce6eogrizs7ovsct5ra/{id}.json';

async function setURI(uri: string) {
  const ERC1155Token = await ethers.getContractFactory('ERC1155Token');
  const contract = ERC1155Token.attach(BSC_CONTRACT_ADDRESS);

  console.log(await contract.setURI(uri));
}

setURI(URI)
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
