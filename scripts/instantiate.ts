import { ethers } from 'hardhat';

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.

  const TOKEN_CONTRACT = await ethers.getContractFactory('ERC1155Token');
  const tokenContract = await TOKEN_CONTRACT.deploy();

  await tokenContract.deployed();

  console.log('Token Contract deployed to:', tokenContract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
