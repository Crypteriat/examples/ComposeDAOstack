import { ethers } from 'hardhat';
import fs from 'fs';
import { getLoadFileData } from '../utils/FileData';
import { balanceOfBatch } from '../utils/ethers';
import nftConfig from '../data/nftConfig';
import { BigNumber } from 'ethers';

const NFT_DATA_FILE = nftConfig.nftLoadFile;
const NFT_OUTPUT_FILE = nftConfig.nftOutputFile;
const BSC_CONTRACT_ADDRESS = nftConfig.bscContractAddr;

// verifies token balances from load file against blockchain
async function verifyTokenBalances(loadFile: string) {
  const FLAGGED_IDS: number[] = [];
  // set up contract interaction
  const ERC1155Token = await ethers.getContractFactory('ERC1155Token');
  const contract = ERC1155Token.attach(BSC_CONTRACT_ADDRESS);

  //make the file stream for storing output data
  const stream = fs.createWriteStream(NFT_OUTPUT_FILE, {
    flags: 'a',
  });

  // get toAddress and tokenIds from loadFile
  const loadFileData = getLoadFileData(loadFile);
  const toAddresses = loadFileData.addresses;
  const tokenIds = loadFileData.tokenIds;

  if (toAddresses.length === tokenIds.length) {
    //make map of addresses and token ids
    const addressMap = new Map(); //bnb address -> tokenIds Array
    addressMap.forEach((toAddress, i) => {
      let ids: number[] = [];
      if (addressMap.has(toAddress)) {
        //get old array if address has been seen before
        ids = addressMap.get(toAddresses[i]);
      } else {
        //create new array if address is new
        ids = [];
      }
      //add id to the array of ids and update map
      ids.push(tokenIds[i]);
      addressMap.set(toAddress, ids);
    });

    console.log(
      '\n///////////////////////////////////////////////////////////////////'
    );
    console.log(
      `\nVerifying token balances from load file: ${NFT_DATA_FILE} against blockchain. 
       \nFull report can be found in: ${NFT_OUTPUT_FILE}
       \nIncorrect balances will be outputted here:\n`
    );

    //loop through map, create address array (of one address) and then get balances
    addressMap.forEach(async (ids: number[], toAddress: string) => {
      //check if valid address
      if (ethers.utils.isAddress(toAddress)) {
        //create array of address the same length as ids array
        const addressArr: string[] = [];
        for (let i = 0; i < ids.length; i++) {
          addressArr.push(toAddress);
        }

        //get the balance and write to out file
        const balances: BigNumber[] = await balanceOfBatch(
          contract,
          addressArr,
          ids
        );

        if (balances.length === ids.length) {
          stream.write(
            `Checking address: ${toAddress} \n token id(s): ${ids} \n`
          );
          stream.write(`token balance(s): ${balances} \n \n`);

          //flag balances not equal to 1, log to console and write to out file
          for (let i = 0; i < balances.length; i++) {
            const currBalance = balances[i].toNumber();
            const currTokenId = ids[i];

            if (currBalance !== 1) {
              console.log(
                `FLAGGED! Caught incorrect balance: ${currBalance} for address: ${toAddress}. tokenId: ${currTokenId}`
              );
              stream.write(
                `FLAGGED! Caught incorrect balance: ${currBalance} for address: ${toAddress}. tokenId: ${currTokenId} \n \n`
              );
              FLAGGED_IDS.push(currTokenId);

              // if token balance equals 0, attempt to find tokenId under different address
              // if (currBalance === 0) {
              //   console.log(
              //     `Checking to see if owned by an address from: ${NFT_DATA_FILE}...`
              //   );
              //   await new Promise((f) => setTimeout(f, 500));
              //   const owner = await findTokenIdOwner(currTokenId, toAddresses);
              //   if (owner.address) {
              //     console.log(
              //       `\nFound balance of: ${owner.tokenBalance} for address: ${owner.address}. Token ID: ${owner.tokenId}\n`
              //     );
              //   } else {
              //     console.log(
              //       `Could not find owner address from: ${NFT_DATA_FILE} for token Id: ${currTokenId}`
              //     );
              //   }
              // }
            }
          }
        } else {
          throw new Error(
            'returned balances array length does not match ids array length'
          );
        }
      } else {
        throw new Error(`invalid address provided. Address: ${toAddress}`);
      }
    });
  } else {
    throw new Error('toAddresses and tokenIds array lengths do not match');
  }

  return FLAGGED_IDS;
}

// Attempts to find the owner of tokenId against addresses from loadFile
async function findTokenIdOwner(tokenId: number, loadFile: string) {
  const loadFileData = await getLoadFileData(loadFile);
  const addresses = loadFileData.addresses;

  const ERC1155Token = await ethers.getContractFactory('ERC1155Token');
  const contract = ERC1155Token.attach(BSC_CONTRACT_ADDRESS);
  let owner = {
    tokenBalance: 0,
    address: '',
    tokenId: -1,
  };

  //check passed parameters
  if (tokenId && addresses && addresses.length > 0) {
    //create array of tokenId the same length as addresses
    const idArr: number[] = [];
    for (let i = 0; i < addresses.length; i++) {
      idArr.push(tokenId);
    }

    if (idArr.length === addresses.length) {
      const balances: BigNumber[] = await balanceOfBatch(
        contract,
        addresses,
        idArr
      );

      for (let i = 0; i < balances.length; i++) {
        const currBalance = balances[i].toNumber();
        if (currBalance !== 0) {
          owner.tokenBalance = currBalance;
          owner.address = addresses[i];
          owner.tokenId = tokenId;
          console.log(
            `\nFound balance of: ${owner.tokenBalance} for address: ${owner.address}. Token ID: ${owner.tokenId}\n`
          );
          return owner;
        }
      }
    } else {
      throw new Error('addresses and tokenIds array lengths do not match');
    }
  } else {
    throw new Error(
      'Error: Bad parameters passed to findTokenIdOwner, check tokenId and addresses.'
    );
  }

  return owner;
}

async function main() {
  const flaggedIds: number[] = await verifyTokenBalances(NFT_DATA_FILE);

  // for (let i = 0; i < flaggedIds.length; i++) {
  //   await findTokenIdOwner(flaggedIds[i], NFT_DATA_FILE);
  // }
}
main();
