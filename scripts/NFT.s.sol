// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import 'forge-std/Script.sol';

import {NFT} from 'contracts/NFT.sol';

contract ContractScript is Script {
  function setUp() public {}

  function run() public {
    vm.broadcast();
    new NFT('ComposeDAOTest', 'CDT', 'BaseURI');
  }
}
