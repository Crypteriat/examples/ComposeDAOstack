import { ethers } from 'hardhat';
import nftConfig from '../data/nftConfig';

const BSC_CONTRACT_ADDRESS = nftConfig.bscTestnetContractAddr;

async function main() {
  // Set up an ethers contract, representing our deployed Box instance
  const ERC1155Token = await ethers.getContractFactory('ERC1155Token');
  const contract = ERC1155Token.attach(BSC_CONTRACT_ADDRESS);

  // console.log(await contract.initialize()); // Only need to call once
  // console.log(await contract.owner()); // Returns owner address

  // console.log(await contract.balanceOf(BSC_OWNER_ADDRESS, 1)); // owner's balance of token id 1
  // console.log(await contract.transferOwnership(BSC_OWNER_ADDRESS));
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
