import hre, { ethers } from 'hardhat';
import fs from 'fs';
import { verifyBscTxn, mintBatch } from '../utils/ethers';
import { getLoadFileData, removeLineFromFile } from '../utils/FileData';
import nftConfig from '../data/nftConfig';

const CHAIN_ID = hre.network.config.chainId;
const NFT_DATA_FILE = nftConfig.nftLoadFile;
const NFT_OUTPUT_FILE = nftConfig.nftOutputFile;
const BSC_CONTRACT_ADDRESS = nftConfig.bscTestnetContractAddr;

async function sendPreSoldNFTs() {
  //make the file stream for storing sent values
  var stream = fs.createWriteStream(NFT_OUTPUT_FILE, {
    flags: 'a',
  });

  // Set up an ethers contract, representing our deployed Box instance
  const ERC1155Token = await ethers.getContractFactory('ERC1155Token');
  const contract = ERC1155Token.attach(BSC_CONTRACT_ADDRESS);

  const loadFileData = getLoadFileData(NFT_DATA_FILE);
  const toAddresses = loadFileData.addresses;
  const tokenIds = loadFileData.tokenIds;
  const toEmails = loadFileData.emails;

  type RowData = {
    email: string;
    id: number;
  };

  if (
    toAddresses.length === tokenIds.length &&
    toEmails.length === toAddresses.length
  ) {
    //make map of addresses and token ids
    const addressMap = new Map(); //bnb address -> RowData Array

    for (let i = 0; i < toAddresses.length; i++) {
      let rowDatas: RowData[] = [];
      if (addressMap.has(toAddresses[i])) {
        //get old array if address has been seen before
        rowDatas = addressMap.get(toAddresses[i]);
      } else {
        //create new array if address is new
        rowDatas = [];
      }
      //add id to the array of ids and update map
      let data: RowData = {
        id: tokenIds[i],
        email: toEmails[i],
      };
      rowDatas.push(data);
      addressMap.set(toAddresses[i], rowDatas);
    }

    console.log('final map: ', addressMap);
    //have to make new arrays since we cant do foreach with await calls
    const mapAddresses: string[] = [];
    const mapRowDatas = [];
    const mapAmounts = [];
    //loop through map and make an array for address, all ids, and total number of nfts
    addressMap.forEach((rowDatas: RowData[], toAddress: string) => {
      // console.log(toAddress, rowDatas);
      mapAddresses.push(toAddress);
      mapRowDatas.push(rowDatas);
      const tArray: number[] = [];
      for (let i = 0; i < rowDatas.length; i++) {
        tArray.push(1);
      }
      mapAmounts.push(tArray);
    });

    for (let i = 0; i < mapAddresses.length; i++) {
      const rowDatas: RowData[] = mapRowDatas[i];
      const toAddress: string = mapAddresses[i];
      let amounts: number[] = mapAmounts[i];
      let ids: number[] = [];

      //get all the ids from the rowData array
      for (let j = 0; j < rowDatas.length; j++) {
        ids.push(rowDatas[j].id);
      }

      //check if valid address
      if (ethers.utils.isAddress(toAddress)) {
        console.log(
          '\n nft ids ' +
            ids +
            ' will be minted to user at address ' +
            toAddress
        );

        //mint
        const transaction = await mintBatch(contract, toAddress, ids, amounts);

        //log, wait 15 seconds, and check result
        console.log(transaction);
        console.log('Waiting for verifiction result...');
        await new Promise((f) => setTimeout(f, 15000));

        const verifiedTxn = await verifyBscTxn(transaction.hash, CHAIN_ID);
        console.log(verifiedTxn);
        //if verified, remove line from file and write transaction data to out file
        if (verifiedTxn) {
          for (let j = 0; j < rowDatas.length; j++) {
            await removeLineFromFile(
              NFT_DATA_FILE,
              rowDatas[j].email,
              rowDatas[j].id,
              toAddress
            );

            const entryData =
              rowDatas[j].email +
              ',' +
              rowDatas[j].id +
              ',' +
              toAddress +
              ',' +
              transaction.hash;

            stream.write(entryData + '\n');
            await new Promise((f) => setTimeout(f, 250));
          }
          stream.write(JSON.stringify(transaction) + '\n');
        } else {
          throw new Error(
            'Transaction did not succeed. Transaction hash: ' + transaction.hash
          );
        }
      } else {
        throw new Error(`invalid address provided. Address: ${toAddress}`);
      }
    }
  } else {
    throw new Error('toAddresses and tokenIds array lengths do not match');
  }

  stream.end();
}

sendPreSoldNFTs()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
