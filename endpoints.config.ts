import * as dotenv from 'dotenv';
import fs from 'fs';
const paths = ['./.env.local', './.env.development', './.env.qa', './.env'];
let path = '';
paths.forEach((p) => {
  if (fs.existsSync(p)) {
    path = p;
  }
});

dotenv.config({ path: path });

export const ALCHEMY_API_KEY = process.env.ALCHEMY_API_KEY;
export const ALCHEMY_BLOCK = process.env.ALCHEMY_BLOCK;
export const ROPSTEN_PRIVATE_KEY = process.env.ROPSTEN_PRIVATE_KEY;
export const KOVAN_PRIVATE_KEY = process.env.KOVAN_PRIVATE_KEY;
export const GOERLI_PRIVATE_KEY = process.env.GOERLI_PRIVATE_KEY;
export const RINKEBY_PRIVATE_KEY = process.env.RINKEBY_PRIVATE_KEY;
export const BNB_TESTNET_PRIVATE_KEY = process.env.BNB_TESTNET_PRIVATE_KEY;
export const BNB_MAINNET_PRIVATE_KEY = process.env.BNB_MAINNET_PRIVATE_KEY;
export const ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY;
export const BSCSCAN_API_KEY = process.env.BSCSCAN_API_KEY;
export const MORALIS_API_KEY = process.env.MORALIS_API_KEY;
export const REPORT_GAS = process.env.REPORT_GAS;
export const REPORT_SIZE = process.env.REPORT_SIZE;

export const FORTMATIC_TEST_API_KEY = process.env.FORTMATIC_TEST_API_KEY;
export const FORTMATIC_LIVE_API_KEY = process.env.FORTMATIC_TEST_LIVE_KEY;
export const MATIC_MUMBAI_MM_ADDRESS = process.env.METAMASK_MATIC_ADDR;
export const PINATA_API_KEY = process.env.PINATA_API_KEY;
export const PINATA_API_SECRET_KEY = process.env.PINATA_API_SECRET_KEY;

const environment = process.env;
export default environment;
