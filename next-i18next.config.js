module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'fr'],
    useSuspense: false,
  },
};
