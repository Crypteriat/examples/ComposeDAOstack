import '../css/tailwind.css';
import React, { useEffect } from 'react';
import Head from 'next/head.js';
import siteMetadata from '@data/siteMetadata';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { appWithTranslation } from 'next-i18next';
import LayoutWrapper from '@components/LayoutWrapper';
import useRouterContext from '../context/RouterContext';
import * as ga from '../data/gtag';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'footer'])),
    },
  };
}

function _App({ Component, pageProps }) {
  const router = useRouterContext();
  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ga.pageview(url);
    };

    // When the component is mounted, subscribe to router changes
    // and log those page views
    router.events.on('routeChangeComplete', handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <>
      <Head>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>
      <LayoutWrapper>
        <Component {...pageProps} />
      </LayoutWrapper>
    </>
  );
}

export default appWithTranslation(_App);
