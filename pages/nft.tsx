import siteMetadata from '@data/siteMetadata';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';
import React from 'react';
import { Web3ModalComponent } from '@components/Web3Modal';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'nft',
        'footer',
        'web3modal',
      ])),
    },
  };
}

export default function NFT() {
  const { t } = useTranslation('nft');

  return (
    <>
      <div className="flex flex-col items-center justify-center mt-12">
        <Web3ModalComponent></Web3ModalComponent>
      </div>
    </>
  );
}
