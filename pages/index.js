import siteMetadata from '@data/siteMetadata';
import Link from '@components/Link';
import BuyWindow from '@components/BuyWindow';
import Image from 'next/image.js';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'react-i18next';
import SimpleImageSlider from 'react-simple-image-slider';
import React, { useState } from 'react';
import { Web3ModalComponent } from '@components/Web3Modal';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'index', 'footer', 'web3modal'])),
    },
  };
}

const IMAGES = [
  { url: '/img/completeNode.jpg' },
  { url: '/img/backside.jpg' },
  { url: '/img/touchscreen.jpg' },
  { url: '/img/completeNode.jpg' },
];

export default function Home() {
  const { t } = useTranslation('index');
  const [isOpen, setIsOpen] = useState(false);

  const toggleBuyWindow = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        {/* <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-400"> </h1> */}
        <div className="flex flex-col items-center justify-center mt-12">
          <SimpleImageSlider
            className="rounded-lg"
            width={540}
            height={360}
            images={IMAGES}
            showBullets={true}
            showNavs={true}
            navStyle={2}
            navSize={30}
            navMargin={20}
          />
          <p className="text-md text-center w-4/6 mt-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus
            nisl urna, aliquet ac ultricies nec, ornare id sem. Sed sit amet
            commodo nulla. Vestibulum semper, ante at lobortis condimentum, urna
            augue imperdiet elit, id rutrum velit nibh ut sem. Sed efficitur
            turpis eget auctor vulputate. Duis placerat et erat ac dapibus.
          </p>
          {/* <button
            className={
              'text-lg font-bold uppercase px-2 py-1 w-56 mt-6 rounded block dark:text-green-600 bg-indigo-400 dark:bg-indigo-900'
            }
            onClick={toggleBuyWindow}
          >
            {t('buy')}
          </button>
          {isOpen && <BuyWindow handleClose={toggleBuyWindow} />} */}
          <Web3ModalComponent></Web3ModalComponent>
        </div>
      </div>
    </>
  );
}
